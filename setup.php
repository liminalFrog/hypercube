<?php

session_start();

require('assets/functions.php');

// Begin setup process
if(!$_SESSION['setup']['step'] || $_SESSION['setup']['step'] == ""){
    unset($_SESSION['setup']);
    $_SESSION['setup']['step'] = 1;
}

$step       = $_SESSION['setup']['step'];
$num_steps  = 4;

$next       = '<div class="d-flex justify-content-between" id="submitBtn">
<a type="button" href="assets/setup.php?clear=1" class="btn btn-secondary px-5">Restart</a>
<input type="submit" class="btn btn-primary px-5" value="Next">
</div>';

$char_msg       = 'Use only valid characters. No characters like: <span class="fw-bold text-monospace">\' ( ] { \\ ; \"</span>';
$char_msg_email = 'Please provide a valid email. (ex. jdoe@example.com)';
$char_msg_phone = 'Use only numbers and parenthesis, dashes/dots. (ex. 000.000.0000 or (000) 000-0000)';
$char_msg_state = 'Only a valid state can be used.';
$char_msg_zip   = 'Use a valid zip code. (00000-0000 or 00000)';
$char_msg_ein   = 'Use a valid EIN number. (00-0000000)';

require('assets/header.php');

open_content();

if($step > 1){
    echo '
    <div class="container">
        <div class="row mb-3">
            <div class="col-lg-8 mx-auto">
                <h1>Installing Hypercube</h1>
                <div class="progress my-3" role="progressbar" aria-label="progress" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" style="width: '.(100 * (($step - 1) / $num_steps)).'%"></div>
                </div>
            
    ';
    if($_SESSION['setup']['err']){
        alert_danger('There were errors with the setup. See below...');
    }
    echo '</div>
        </div>
    </div>';

}

switch($step){

    // STEP 1: INTRO
    case 1:

?>
<div class="px-4 py-5 my-5 text-center">
    <img class="d-block mx-auto mb-4" src="images/logo-md.png" alt="" width="100">
    <h1 class="display-5">Hypercube</h1>
    <div class="col-lg-6 mx-auto">
        <p class="lead mb-4">Welcome to Hypercube ERP: an Open Source system for managing your business. Get started by
            clicking the button below.</p>
        <div class="d-grid d-sm-flex justify-content-sm-center">
            <a href="assets/setup.php" type="button" class="btn btn-primary btn-lg px-4 gap-3">Install Hypercube</a>
        </div>
    </div>
</div>
<?php
    break;

    // STEP 2: DATABASE INFORMATION:
    case 2:

?>

<div class="container">
    <div class="row my-3">
        <div class="col-lg-8 mx-auto">

            <h3>Database Information</h3>
            <form action="assets/setup.php" class="my-3" method="post" id="form">
                <?php
                // If could not connect to database
                if ($_SESSION['setup']['err']['connect_error']) {
                    alert_danger('<b>Error:</b> Could not connect to database. Make sure your credentials were correct.');
                }

                // If could not write config file
                if ($_SESSION['setup']['err']['nowrite'] || $nowrite) {
                    alert_danger('<b>Error:</b> Could not write config file. Make sure you have write permissions. From your web root folder, type:
                    <pre class="mb-0 mt-2">sudo chmod 777 ../</pre>');
                }

                // If could not connect to database with config file
                if ($_SESSION['setup']['err']['config_connect']) {
                    alert_danger('<b>Error:</b> Could not connect to database with config file. Do you have write permissions in your web root folder\'s parent directory?');
                }

                // Check write permissions for parent dir. DB config will go there.
                $dir = '../';
                if(is_dir($dir)){
                    if(!is_writable($dir)){
                        alert_warning('Hypercube detects that you do not have write permissions for the parent directory.');
                    } else {
                       // alert_success('&check; You have write permissions.');
                    }
                }
                ?>
                <div class="input-group has-validation mb-3">
                    <span class="input-group-text rounded-0 rounded-start">Hostname</span>
                    <input type="text" name="hostname" id="hostname"<?php
                        if ($_SESSION['setup']['err'] && !$_SESSION['setup']['err']['hostname_invalid']) {
                            echo ' value="'.sec($_SESSION['setup']['hostname']).'"';
                        }
                    ?> aria-describedby="ivfb_hostname" class="form-control<?php
                    if ($_SESSION['setup']['err'] && 
                    ($_SESSION['setup']['err']['hostname_empty'] ||
                    $_SESSION['setup']['err']['hostname_invalid'])) {
                        echo " is-invalid";
                    }
                    ?>" autofocus>
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback" id="ivfb_hostname">';
                            if ($_SESSION['setup']['err']['hostname_empty']) {
                                echo 'Please provide a hostname.';
                            } else if ($_SESSION['setup']['err']['hostname_invalid']) {
                                echo $char_msg;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="input-group has-validation mb-3">
                    <span class="input-group-text rounded-0 rounded-start">Username</span>
                    <input type="text" name="username" id="username"<?php
                        if ($_SESSION['setup']['err'] && !$_SESSION['setup']['err']['username_invalid']) {
                            echo ' value="'.sec($_SESSION['setup']['username']).'"';
                        }
                    ?> aria-describedby="ivfb_username" class="form-control<?php
                    if ($_SESSION['setup']['err'] && ($_SESSION['setup']['err']['username_empty'] ||
                    $_SESSION['setup']['err']['username_invalid'])) {
                        echo " is-invalid";
                    }
                    ?>">
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback" id="ivfb_username">';
                            if ($_SESSION['setup']['err']['username_empty']) {
                                echo 'Please provide a username.';
                            } else if ($_SESSION['setup']['err']['username_invalid']) {
                                echo $char_msg;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="input-group has-validation mb-3">
                    <span class="input-group-text rounded-0 rounded-start">Password</span>
                    <input type="password" name="password" id="password"<?php
                        if ($_SESSION['setup']['err'] && !$_SESSION['setup']['err']['password_invalid']) {
                            echo ' value="'.sec($_SESSION['setup']['password']).'"';
                        }
                    ?> aria-describedby="ivfb_password" class="form-control<?php
                    if ($_SESSION['setup']['err'] && ($_SESSION['setup']['err']['password_empty'] ||
                    $_SESSION['setup']['err']['password_invalid'])) {
                        echo " is-invalid";
                    }
                    ?>">
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback" id="ivfb_password">';
                            if ($_SESSION['setup']['err']['password_empty']) {
                                echo 'Please provide a password.';
                            } else if ($_SESSION['setup']['err']['password_invalid']) {
                                echo $char_msg;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="input-group has-validation mb-3">
                    <span class="input-group-text rounded-0 rounded-start">Database Name</span>
                    <input type="text" name="database" id="database"<?php
                        if ($_SESSION['setup']['err'] && !$_SESSION['setup']['err']['database_invalid']) {
                            echo ' value="'.sec($_SESSION['setup']['database']).'"';
                        }
                    ?> aria-describedby="ivfb_database" class="form-control<?php
                    if ($_SESSION['setup']['err'] && ($_SESSION['setup']['err']['database_empty'] ||
                    $_SESSION['setup']['err']['database_invalid'])) {
                        echo " is-invalid";
                    }
                    ?>">
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback" id="ivfb_database">';
                            if ($_SESSION['setup']['err']['database_empty']) {
                                echo 'Please provide a database.';
                            } else if ($_SESSION['setup']['err']['database_invalid']) {
                                echo $char_msg;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <?php echo $next; ?>
            </form>
            <script>
                $("form").submit(function(event){
                    $("#submitBtn").html('<button class="btn btn-primary" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...</button>');
                });
            </script>
        </div>
    </div>
</div>

<?php
    break;

    // COMPANY INFO
    case 3:
?>

<div class="container">
    <div class="row my-3">
        <div class="col-lg-8 mx-auto">

            <h2>Company Information</h2>
            <pre>* required</pre>
            <form action="assets/setup.php" class="row g-3" method="post">
                <div class="input-group input-group-lg has-validation mb-3">
                <span class="input-group-text rounded-0 rounded-start">Company Name *</span>
		        <input type="text" name="company_name" id="company_name" class="form-control<?php
                if (
                    $_SESSION['setup']['err'] &&
                    ($_SESSION['setup']['err']['company_name_empty'] ||
                    $_SESSION['setup']['err']['company_name_invalid'])
                ) {
                    echo ' is-invalid';
                }
                ?>" placeholder="Name of company" autofocus<?php
                if ($_SESSION['setup']['company_name'] && $_SESSION['setup']['company_name'] != "") {
                    echo ' value="'.sec($_SESSION['setup']['company_name']).'"';
                }
                ?>>
                <?php
                    if ($_SESSION['setup']['err']) {
                        echo '<div class="invalid-feedback" id="ivfb_database">';
                        if ($_SESSION['setup']['err']['company_name_empty']) {
                            echo 'Please provide a company name.';
                        } else if ($_SESSION['setup']['err']['company_name_invalid']) {
                            echo $char_msg;
                        }
                        echo '</div>';
                    }
                ?>
                </div>
                
                <div class="col-12 input-group has-validation">
                    <span class="input-group-text rounded-0 rounded-start">Address</span>
                    <input type="text" class="form-control<?php
                    if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_address1_invalid']) {
                        echo ' is-invalid';
                    }
                    ?>" name="company_address1" id="company_address1" placeholder="1234 Main St"<?php
                    if ($_SESSION['setup']['company_address1'] && $_SESSION['setup']['company_address1'] != "") {
                        echo ' value="'.sec($_SESSION['setup']['company_address1']).'"';
                    }
                    ?>>
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback">';
                            if ($_SESSION['setup']['err']['company_address1_invalid']) {
                                echo $char_msg;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="col-12 input-group has-validation">
                    <span class="input-group-text rounded-0 rounded-start">Address 2</span>
                    <input type="text" class="form-control<?php
                    if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_address2_invalid']) {
                        echo ' is-invalid';
                    }
                    ?>" name="company_address2" id="company_address2" placeholder="Apartment, studio, or floor"<?php
                    if ($_SESSION['setup']['company_address2'] && $_SESSION['setup']['company_address2'] != "") {
                        echo ' value="'.sec($_SESSION['setup']['company_address2']).'"';
                    }
                    ?>>
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback">';
                            if ($_SESSION['setup']['err']['company_address2_invalid']) {
                                echo $char_msg;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="col-md-5">
                    <div class="input-group has-validation">
                        <span class="input-group-text rounded-0 rounded-start">City</span>
                        <input type="text" class="form-control<?php
                        if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_city_invalid']) {
                            echo ' is-invalid';
                        }
                        ?>" name="company_city" id="company_city"<?php
                        if ($_SESSION['setup']['company_city'] && $_SESSION['setup']['company_city'] != "") {
                            echo ' value="'.sec($_SESSION['setup']['company_city']).'"';
                        }
                        ?>>
                        <?php
                            if ($_SESSION['setup']['err']) {
                                echo '<div class="invalid-feedback">';
                                if ($_SESSION['setup']['err']['company_city_invalid']) {
                                    echo $char_msg;
                                }
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group has-validation">
                        <span class="input-group-text rounded-0 rounded-start">State</span>
                        <select name="company_state" id="company_state" class="form-select<?php
                        if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_state_invalid']) {
                            echo ' is-invalid';
                        }
                        ?>">
                            <option selected disabled>Select state</option>
                        <?php
                        foreach (states_array() as $code => $state_name) {
                            echo '<option value="'.$code.'"';
                            if ($_SESSION['setup']['company_state'] && $_SESSION['setup']['company_state'] != ""){
                                if ($_SESSION['setup']['company_state'] == $code) {
                                    echo ' selected';
                                }
                            }
                            echo '>'.$state_name.'</option>
                            ';
                        }
                        ?>
                        </select>
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback">';
                            if ($_SESSION['setup']['err']['company_state_invalid']) {
                                echo $char_msg_state;
                            }
                            echo '</div>';
                        }
                    ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group has-validation">
                        <span class="input-group-text rounded-0 rounded-start">Zip</span>
                        <input type="text" class="form-control<?php
                        if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_zip_invalid']) {
                            echo ' is-invalid';
                        }
                        ?>" name="company_zip" id="company_zip"<?php
                        if ($_SESSION['setup']['company_zip'] && $_SESSION['setup']['company_zip'] != "") {
                            echo ' value="'.sec($_SESSION['setup']['company_zip']).'"';
                        }
                        ?>>
                        <?php
                            if ($_SESSION['setup']['err']) {
                                echo '<div class="invalid-feedback">';
                                if ($_SESSION['setup']['err']['company_zip_invalid']) {
                                    echo $char_msg_zip;
                                }
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>

                <br>
                <div class="input-group has-validation mb-3">
                    <span class="input-group-text rounded-0 rounded-start">@ Main Email</span>
                    <input type="text" name="company_email" id="company_email" class="form-control<?php
                    if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_email_invalid']) {
                        echo ' is-invalid';
                    }
                    ?>"<?php
                    if ($_SESSION['setup']['company_email'] && $_SESSION['setup']['company_email'] != "") {
                        echo ' value="'.sec($_SESSION['setup']['company_email']).'"';
                    }
                    ?>>
                    <?php
                        if ($_SESSION['setup']['err']) {
                            echo '<div class="invalid-feedback">';
                            if ($_SESSION['setup']['err']['company_email_invalid']) {
                                echo $char_msg_email;
                            }
                            echo '</div>';
                        }
                    ?>
                </div>
                <div class="col-md-6">
                    <div class="input-group has-validation mb-3">
                        <span class="input-group-text rounded-0 rounded-start"><i class="bi-telephone pe-2"></i>Main Phone</span>
                        <input type="text" name="company_phone" id="company_phone" class="form-control<?php
                        if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_phone_invalid']) {
                            echo ' is-invalid';
                        }
                        ?>"<?php
                        if ($_SESSION['setup']['company_phone'] && $_SESSION['setup']['company_phone'] != "") {
                            echo ' value="'.sec($_SESSION['setup']['company_phone']).'"';
                        }
                        ?>>
                        <?php
                            if ($_SESSION['setup']['err']) {
                                echo '<div class="invalid-feedback">';
                                if ($_SESSION['setup']['err']['company_phone_invalid']) {
                                    echo $char_msg_phone;
                                }
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="input-group has-validation mb-3">
                        <span class="input-group-text rounded-0 rounded-start"><i class="bi-printer pe-2"></i>Fax</span>
                        <input type="text" name="company_fax" id="company_fax" class="form-control<?php
                        if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_fax_invalid']) {
                            echo ' is-invalid';
                        }
                        ?>"<?php
                        if ($_SESSION['setup']['company_fax'] && $_SESSION['setup']['company_fax'] != "") {
                            echo ' value="'.sec($_SESSION['setup']['company_fax']).'"';
                        }
                        ?>>
                        <?php
                            if ($_SESSION['setup']['err']) {
                                echo '<div class="invalid-feedback">';
                                if ($_SESSION['setup']['err']['company_fax_invalid']) {
                                    echo $char_msg_phone;
                                }
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>

                <!-- EIN -->
                <div class="col-md-6">
                    <label for="company_ein" class="form-label">Employer Identification Number</label>
                    <div class="input-group has-validation mb-3">
                        <span class="input-group-text rounded-0 rounded-start">EIN</span>
                        <input type="text" name="company_ein" id="company_ein" class="form-control<?php
                        if ($_SESSION['setup']['err'] && $_SESSION['setup']['err']['company_ein_invalid']) {
                            echo ' is-invalid';
                        }
                        ?>"<?php
                        if ($_SESSION['setup']['company_ein'] && $_SESSION['setup']['company_ein'] != "") {
                            echo ' value="'.sec($_SESSION['setup']['company_ein']).'"';
                        }
                        ?>>
                        <?php
                            if ($_SESSION['setup']['err']) {
                                echo '<div class="invalid-feedback">';
                                if ($_SESSION['setup']['err']['company_ein_invalid']) {
                                    echo $char_msg_ein;
                                }
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>
                <?php echo $next; ?>
            </form>
        </div>
    </div>
</div>

<?php

    break;

    default:

    ?>
<div class="container">
    <div class="row my-3">
        <div class="col-lg-8">
            <h1>Oops!</h1>
            <h4>Something went wrong.</h4>
            <a href="setup.php" class="btn btn-primary" type="button">Start Over</a>
        </div>
    </div>
</div>
<?php
    break;
}

close_content();
require('assets/footer.php');

?>
