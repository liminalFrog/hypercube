<?php
// Invoice View

// Get assets
require('assets/start.php');

// Sub navbar
$subnav = array(
  'View Invoices'    => 'invoices.php',
  'Add Invoice' => 'invoice-add.php',
);
subnav($subnav);

open_content();

page_title("Invoice INV-10032");
nav_active('crm');

// Get invoice data
$invoice_sql  = 'SELECT
  i_id,
  i.i_amount,
  c_fname, 
  c_lname, 
  c_address1,
  c_address2,
  c_email,
  i_discount, 
  i_discount_per, 
  i_tax, 
  i_tax_per, 
  istat_name, 
  istat_color, 
  i_paymethod, 
  i_terms, 
  DATE_FORMAT(i_date, "%m-%d-%Y") AS i_date, 
  DATE_FORMAT(i_duedate, "%m-%d-%Y") AS i_duedate, 
  i_notes_pub, 
  i_notes_pri 
FROM invoices AS i 
  LEFT OUTER JOIN customers AS c ON i.c_id = c.c_id 
  LEFT OUTER JOIN invoice_statuses AS istat ON i.istat_id = istat.istat_id 
WHERE i.i_id = '.$i_id.';';

// Put data into $invoice
$invoice = getdata($invoice_sql);



// Begin subtotal count
(float)$subtotal = 0;

?>

<style>
    body {
      .d-print-none;
    }
</style>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3 d-print-none">
    <p class="my-0">
      <span class="badge text-secondary">Status:</span> <span class="badge bg-<?php echo $invoice['istat_color']; ?>"><?php echo $invoice['istat_name']; ?></span>
    </p>
  </div>
  <div class="row mb-3 d-print-none">
    <div class="btn-toolbar justify-content-between">
      <div class="btn-group me-2">
        <a class="btn btn-sm btn-outline-secondary" role="button" href="#" onclick="window.print()"><i class="bi-printer"></i> Print</a>
        <a class="btn btn-sm btn-outline-secondary" role="button" href="#" data-bs-toggle="modal" data-bs-target="#sendModal"><i class="bi-envelope"></i> Send to Customer</a>
        <a class="btn btn-sm btn-outline-secondary" role="button" href="invoice-edit.php?i_id=10032"><i class="bi-pencil"></i> Edit</a>
        <a class="btn btn-sm btn-outline-secondary" role="button" href="job-view.php?j_id=1"><i class="bi-tools"></i> Go to Job</a>
      </div>

      <div class="btn-group">
        <a class="btn btn-sm btn-outline-danger" role="button" href="#" data-bs-toggle="modal" data-bs-target="#sendModal"><i class="bi-cash-coin"></i> Refund</a>
        <a class="btn btn-sm btn-outline-success" role="button" href="#" data-bs-toggle="modal" data-bs-target="#sendModal"><i class="bi-cash-coin"></i> Add Payment</a>
      </div>
    </div>

  </div>
  <!-- Send to customer modal -->
  <div class="modal fade" id="sendModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Send Invoice Customer</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <strong>Confirm:</strong> Send invoice INV-<?php echo $invoice['i_id']; ?> to <?php echo $invoice['c_fname'].' '.$invoice['c_lname']; ?>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-sm btn-success" data-bs-dismiss="modal" onclick="sendInvoice()">Send Invoice</button>
        </div>
      </div>
    </div>
  </div>

  <div class="row mb-3 d-print-block">
    <div class="row mb-3 border-bottom border-<?php echo $invoice['istat_color']; ?> border-1">
      <div class="col-lg-6 col-sm-6 w-50">
        <h3 class="invoice-header text-uppercase text-<?php echo $invoice['istat_color']; ?>">Invoice</h3>
      </div>
      <div class="col-lg-6 col-sm-6 w-50 text-end">
        <h3 class="text-<?php echo $invoice['istat_color']; ?>">INV-<?php echo $invoice['i_id']; ?></h3>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-lg-6 col-sm-6 w-50">
        <p class="mb-3">
          Invoice Date: <?php echo $invoice['i_date']; ?><br>
          Due Date: <span class="text-<?php echo $invoice['istat_color']; ?>"><?php echo $invoice['i_duedate']; ?></span>
        </p>
        <p class="mt-4"><strong class="text-<?php echo $invoice['istat_color']; ?>"><?php echo $invoice['c_fname'].' '.$invoice['c_lname']; ?></strong>
          <?php
            // Customer Address, if any
            if($invoice['c_address1'] && $invoice['c_address1'] != "")
              echo '<br>'.$invoice['c_address1'];
            if($invoice['c_address2'] && $invoice['c_address2'] != "")
              echo '<br>'.$invoice['c_address2'];
          ?>
        </p>
      </div>
      <div class="col-lg-6 col-sm-6 w-50 text-end ">
        <p>
          <strong>Trinity Metalworks, LLC</strong><br>
          10032 SE County Road 4200<br>
          Kerens, TX 75144
        </p>
        <p>
          info@trinitymetalworks.com
        </p>
        <p>
          (000) 123-4567<br>
          (123)456-7890
        </p>
      </div>
    </div>
    <div class="row mb-3">
      <table class="table text-end">
        <thead>
          <tr>
            <th scope="col" class="text-start">Description</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
            <th scope="col">Amount</th>
          </tr>
        </thead>
        <tbody>
          <?php

// LINE ITEMS

$line_itmes_count_sql = 'SELECT COUNT(ili_id) AS count FROM invoice_line_items WHERE i_id = '.$i_id.';';
$count_data = getdata($line_itmes_count_sql);
$count = $count_data['count'];

$line_item_sql = 'SELECT ili_id, ili_amount, ili_tax, ili_tax_per, ili_discount, ili_discount_per, ili_qty, p_id, ili_name, ili_desc FROM invoice_line_items WHERE i_id = '.$i_id.';';
$result = $db->query($line_item_sql);
while ($data = $result->fetch_array()){
  echo '<tr><td class="text-start">'.$data['ili_name'].'<br>';

  // Make line item total and add to invoice subtotal
  $ili_total = $data['ili_qty'] * $data['ili_amount'];
  $subtotal += $ili_total;

  if($data['ili_desc'] && $data['ili_desc'] != "")
    echo '<span class="badge text-secondary">'.$data['ili_desc'].'</span>';
  echo '</td>
  <td>'.$data['ili_qty'].'</td>
  <td>'.$data['ili_amount'].'</td>
  <td>'.currency($ili_total).'</td>
  </tr>';
}

          ?><tr>
            <td colspan="2"></td>
            <td>Subtotal</td>
            <td>$<?php
            // TOTAL AMOUNT CALCULATION

            // If amount recorded is different from current total of line items' prices
            if ($subtotal != $invoice['i_amount']){
              text_danger(currency($subtotal));
            } else if ($subtotal == $invoice['i_amount']){
              echo commas(currency($subtotal));
            }

            ?></td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Discount</td>
            <td>
              $<?php echo currency(fill_zero($invoice['i_discount'])); ?><br>
              <span class="badge text-secondary">%<?php echo currency(fill_zero($invoice['i_discount_per'])); ?></span>
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Sales Tax (%<?php echo currency(fill_zero($invoice['i_tax_per'])); ?>)</td>
            <td>
              $<?php echo currency(fill_zero($invoice['i_tax'])); ?>
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Total</td>
            <td>
              <?php
                $total = $subtotal - $invoice['i_discount'] + $invoice['i_tax'];
                echo commas(currency($total));
              ?>
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Paid</td>
            <td>
              $0.00
            </td>
          </tr>
          <tr class="border-bottom border-<?php echo $invoice['istat_color']; ?>">
            <td colspan="3" class="fw-bold text-<?php echo $invoice['istat_color']; ?>">Balance Due</td>
            <td class="fw-bold text-<?php echo $invoice['istat_color']; ?>">
              $8,668.50
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="row mb-3 text-secondary fst-italic">
      <strong>Notes (private):</strong><br>
      <?php echo $invoice['i_notes_pri']; ?>
    </div>
  </div>
</div>

<!-- Toasts -->
<div class="toast-container position-fixed bottom-0 end-0 p-3">
  <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <img src="..." class="rounded me-2" alt="...">
      <strong class="me-auto">Bootstrap</strong>
      <small>11 mins ago</small>
      <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body">
      Hello, world! This is a toast message.
    </div>
  </div>

  <div id="sendToast" class="toast align-items-center text-light border-0 bg-success" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="d-flex">
      <div class="toast-body">
        Invoice successfully sent!
      </div>
      <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
  </div>
</div>

<script>
const toastTrigger = document.getElementById('liveToastBtn')
const toastLiveExample = document.getElementById('sendToast')
function sendInvoice(){
  const toast = new bootstrap.Toast(toastLiveExample)

  toast.show()
}


</script>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
