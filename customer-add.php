
<?php
// Main file

// Get assets
require('assets/start.php');

// Sub navbar
$subnav = array(
  'View Customers'    => 'customers.php',
  'Add Customer' => 'customer-add.php',
);
subnav($subnav);

open_content();

echo page_title("Add Customer");
nav_active('crm');

?>

<form action="assets/customer-add.php" method="post" class="row needs-validation" novalidate>

<div class="container-fluid">
  <div class="row">
    <div class="col-lg-6 mb-3">
      <div class="row mb-1">
        <div class="col-md-6">
          <label for="c_fname" class="form-label">First Name  <span class="text-danger">*</span></label>
          <input name="c_fname" type="text" class="form-control" placeholder="First Name" autocomplete="off" required autofocus>
          <div class="invalid-feedback">
            Enter first name.
          </div>
        </div>
        <div class="col-md-6">
          <label for="c_lname" class="form-label">Last Name  <span class="text-danger">*</span></label>
          <input name="c_lname" type="text" class="form-control" placeholder="Last Name" autocomplete="off" required>
          <div class="invalid-feedback">
            Enter last name.
          </div>
        </div>
      </div>
      <div class="mb-5">
        <label for="c_company" class="form-label">Company</label>
        <input name="c_company" type="text" class="form-control" placeholder="Company name" autocomplete="off">
      </div>

      <div class="mb-5">
        <label for="c_email" class="form-label">Email Address <span class="text-danger">*</span></label>
        <input name="c_email" type="text" class="form-control" placeholder="Email address" autocomplete="off" required>
        <div class="invalid-feedback">
          Enter email address.
        </div>
      </div>

      <div class="mb-1">
        <label for="c_cphone" class="form-label">Cell <span class="text-danger">*</span></label>
        <input name="c_cphone" type="text" class="form-control" placeholder="Cell number" autocomplete="off" required>
        <div class="invalid-feedback">
          Enter cell phone number.
        </div>
      </div>
      <div class="mb-1">
        <label for="c_wphone" class="form-label">Work</label>
        <input name="c_wphone" type="text" class="form-control" placeholder="Work number" autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="c_hphone" class="form-label">Home</label>
        <input name="c_hphone" type="text" class="form-control" placeholder="Home number" autocomplete="off">
      </div>
      <div class="mb-5">
        <label for="c_fax" class="form-label">Fax</label>
        <input name="c_fax" type="text" class="form-control" placeholder="Fax number" autocomplete="off">
      </div>

      <div class="mb-1">
        <label for="c_address1" class="form-label">Address 1</label>
        <input name="c_address1" type="text" class="form-control" placeholder="1234 Main St." autocomplete="off">
      </div>
      <div class="mb-1">
        <label for="c_address2" class="form-label">Address 2</label>
        <input name="c_address2" type="text" class="form-control" placeholder="Apartment, studio, or floor" autocomplete="off">
      </div>
      <div class="row mb-3">
        <div class="col-md-6">
          <label class="form-label" for="c_city">City</label>
          <input type="text" class="form-control" id="c_city" placeholder="City name" autocomplete="off">
        </div>
        <div class="col-md-3">
          <label for="c_state" class="form-label">State</label>
          <select id="c_state" class="form-select">
            <option selected disabled>State</option>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
          </select>
        </div>
        <div class="col-md-3">
          <label for="c_zip" class="form-label">Zip Code</label>
          <input type="text" class="form-control" id="c_zip" placeholder="00000-0000" autocomplete="off">
        </div>
      </div>

    </div>
    <div class="col-lg-6">
      <div class="mb-5">
        <label for="c_notes" class="form-label">Notes</label>
        <textarea class="form-control" aria-label="Notes" name="c_notes"></textarea>
      </div>
    </div>
  </div>
  <div class="row mb-1">
    <div class="col-2">
      <input type="submit" class="form-control btn btn-primary" value="Submit" />
    </div>
  </div>
</div>

</form>

<?php

validation();

close_content();

// Get footer
require('assets/footer.php');

?>
