<?php
// Jobs

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'View Jobs'    => 'jobs.php',
  'Add Job' => 'job-add.php',
);
subnav($subnav);

open_content();

echo page_title("Jobs");
nav_active('crm');

?>

<div class="container-fluid" id="containerWidth">
  <div class="row"><!-- Main container row -->

    <!-- Jobs panels -->
    <div class="col-md-4">
      <div class="row mb-3">
        <!-- Open Jobs -->
        <div class="col-12">
          <label for="openJobsPanel" class="form-label">Open Jobs</label>
          <div id="openJobsPanel" class="list-group shadow-sm">
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span><br>
              Deadline: <strong class="text-danger">3 days</strong>
            </a>
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span><br>
              Deadline: <strong class="">7 days</strong>
            </a>
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span><br>
              Deadline: <strong class="">12 days</strong>
            </a>
          </div>
        </div>
      </div>
      <div class="row mb-3">
        <!-- Upcoming Jobs -->
        <div class="col-12">
          <label for="upcomingJobsPanel" class="form-label">Upcoming Jobs</label>
          <div id="upcomingJobsPanel" class="list-group shadow-sm">
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span><br>
              Start: <strong class="text-warning">3 days from now</strong>
            </a>
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span><br>
              Start: <strong class="">7 days from now</strong>
            </a>
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span><br>
              Start: <strong class="">12 days from now</strong>
            </a>
          </div>
        </div>
      </div>
      <div class="row mb-3">
        <!-- Pending Jobs -->
        <div class="col-12">
          <label for="pendingJobsPanel" class="form-label">Pending Jobs</label>
          <div id="pendingJobsPanel" class="list-group shadow-sm">
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span>
            </a>
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span>
            </a>
            <a class="list-group-item list-group-item-action" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span>
            </a>
          </div>
        </div>
      </div>
      <div class="row mb-3">
        <!-- Cancelled Jobs -->
        <div class="col-12">
          <label for="cancelledJobsPanel" class="form-label">Cancelled Jobs</label>
          <div id="cancelledJobsPanel" class="list-group shadow-sm">
            <a class="list-group-item list-group-item-action text-muted" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span>
            </a>
            <a class="list-group-item list-group-item-action text-muted" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span>
            </a>
            <a class="list-group-item list-group-item-action text-muted" href="job-view.php?j_id=1">
              <strong>30 Head Corral</strong><br>
              <span class="text-muted">John Doe</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- Stats -->
    <div class="col-md-8">
      <div class="row mb-3">
        <nav class="nav nav-tabs">
          <a class="nav-link" href="#" onclick="statsChange(this.value)" value="week">Week</a>
          <a class="nav-link active" href="#" onclick="statsChange(this.value)" value="month">Month</a>
          <a class="nav-link" href="#" onclick="statsChange(this.value)" value="year">Year</a>
        </nav>
      </div>
      <div class="row mb-1">
        <div class="col-md-4">
          <label for="jobsThisMonth" class="form-label">Jobs this Month</label>
          <h4 id="jobsThisMonth">13</h4>
        </div>

        <div class="col-md-4">
          <label for="incomeThisMonth" class="form-label">Incoming this Month</label>
          <h4 id="incomeThisMonth">$23,237<span class="fs-6">.00</span></h4>
        </div>

        <div class="col-md-4">
          <label for="jobsThisMonth" class="form-label">Completed</label>
          <h4 id="incomeThisMonth">$23,237<span class="fs-6">.00</span></h4>
        </div>
        <hr />
      </div>

      <!-- Chart -->
      <div class="row mb-3">
        <div class="col">
          <div class="row">
            <div class="col">
              <strong class="badge bg-light text-muted"><i class="bi-circle-fill text-primary"></i> In Progress</strong>
              <strong class="badge bg-light text-muted"><i class="bi-circle-fill text-success"></i> Completed</strong>
            </div>
          </div>

          <div class="row">
            <style>
              .progress {
                height: 10px;
              }
            </style>
            <table class="table border-light">
              <thead class="p-0">
                <tr class="text-muted text-end">
                  <th scope="col" class="pe-0 w-25">25</th>
                  <th scope="col" class="pe-0 w-25">50</th>
                  <th scope="col" class="pe-0 w-25">75</th>
                  <th scope="col" class="pe-0 w-25">100</th>
                </tr>
              </thead>
              <tbody class="border-top-0">
                <tr>
                  <td colspan="4" class="p-0">
                    <span class="badge bg-light text-dark">May</span><br>
                    <div class="progress p-0 mb-1">
                      <div class="progress-bar" style="width:48%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="progress p-0">
                      <div class="progress-bar bg-success" style="width:52%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colspan="4" class="p-0">
                    <span class="badge bg-light text-dark">April</span><br>
                    <div class="progress p-0 mb-1">
                      <div class="progress-bar" style="width:48%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="progress p-0">
                      <div class="progress-bar bg-success" style="width:52%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colspan="4" class="p-0">
                    <span class="badge bg-light text-dark">March</span><br>
                    <div class="progress p-0 mb-1">
                      <div class="progress-bar" style="width:48%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="progress p-0">
                      <div class="progress-bar bg-success" style="width:52%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colspan="4" class="p-0">
                    <span class="badge bg-light text-dark">February</span><br>
                    <div class="progress p-0 mb-1">
                      <div class="progress-bar" style="width:48%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="progress p-0">
                      <div class="progress-bar bg-success" style="width:52%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colspan="4" class="p-0">
                    <span class="badge bg-light text-dark">January</span><br>
                    <div class="progress p-0 mb-1">
                      <div class="progress-bar" style="width:48%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="progress p-0">
                      <div class="progress-bar bg-success" style="width:52%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<?php

close_content();

// Get footer
require('assets/footer.php');

?>
