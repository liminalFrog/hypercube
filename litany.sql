-- MariaDB dump 10.19  Distrib 10.9.4-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: litany
-- ------------------------------------------------------
-- Server version	10.9.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitcoin_received`
--

DROP TABLE IF EXISTS `bitcoin_received`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitcoin_received` (
  `btcr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `btcr_trans_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`btcr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitcoin_received`
--

LOCK TABLES `bitcoin_received` WRITE;
/*!40000 ALTER TABLE `bitcoin_received` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitcoin_received` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checks_received`
--

DROP TABLE IF EXISTS `checks_received`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checks_received` (
  `chkr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chkr_num` int(11) NOT NULL,
  `chkr_notes` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`chkr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checks_received`
--

LOCK TABLES `checks_received` WRITE;
/*!40000 ALTER TABLE `checks_received` DISABLE KEYS */;
/*!40000 ALTER TABLE `checks_received` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_events`
--

DROP TABLE IF EXISTS `customer_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_events` (
  `ce_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ce_name` varchar(50) DEFAULT NULL,
  `ce_description` text DEFAULT NULL,
  `ce_dateadded` datetime DEFAULT NULL,
  `ce_datetime` datetime DEFAULT NULL,
  `ces_id` int(11) DEFAULT NULL,
  `c_id` int(11) DEFAULT NULL,
  `j_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ce_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_events`
--

LOCK TABLES `customer_events` WRITE;
/*!40000 ALTER TABLE `customer_events` DISABLE KEYS */;
INSERT INTO `customer_events` VALUES
(1,'Phone call','Remeber to talk to John about panel length','2022-05-17 05:42:29','2022-05-17 05:42:29',NULL,1,NULL);
/*!40000 ALTER TABLE `customer_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `c_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_fname` varchar(25) DEFAULT NULL,
  `c_lname` varchar(35) DEFAULT NULL,
  `c_company` varchar(50) DEFAULT NULL,
  `c_cphone` varchar(12) DEFAULT NULL,
  `c_hphone` varchar(12) DEFAULT NULL,
  `c_wphone` varchar(12) DEFAULT NULL,
  `c_fax` varchar(12) DEFAULT NULL,
  `c_email` varchar(40) DEFAULT NULL,
  `c_address1` varchar(100) DEFAULT NULL,
  `c_address2` varchar(100) DEFAULT NULL,
  `c_city` varchar(35) DEFAULT NULL,
  `c_state` char(2) DEFAULT NULL,
  `c_zip` varchar(10) DEFAULT NULL,
  `cs_id` int(11) DEFAULT NULL,
  `ct_id` int(11) DEFAULT NULL,
  `c_dateadded` datetime DEFAULT NULL,
  `c_notes` text DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES
(1,'Adam','Mureiko',NULL,'0000000000',NULL,NULL,NULL,'adam@trinitymetalworks.com','10032 SE County Road 4200',NULL,'Kerens','TX','75144',NULL,NULL,'2022-09-01 16:46:53',NULL),
(2,'Carl','Frederickson','CFCorp, Ltd.','2213244894',NULL,NULL,NULL,'carl@example.com','CFCorp Headquarters','0003 Nowhere St.','Notown','TX','00000-0000',1,NULL,'2022-08-08 14:16:33','These are his notes.'),
(3,'John','Doe','jDoe, Inc.','13748293843',NULL,NULL,NULL,'john@example.com','487 Anywhere Rd.',NULL,'Anyplace','TX','00000-0000',1,NULL,'2022-08-08 14:16:33',NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_line_items`
--

DROP TABLE IF EXISTS `invoice_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_line_items` (
  `ili_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ili_amount` float(10,2) DEFAULT NULL,
  `ili_tax` float(11,2) DEFAULT NULL,
  `ili_tax_per` float(4,2) DEFAULT NULL,
  `ili_discount` float(11,2) DEFAULT NULL,
  `ili_discount_per` float(4,2) DEFAULT NULL,
  `ili_qty` int(11) DEFAULT NULL,
  `i_id` int(11) DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL,
  `ili_name` varchar(50) DEFAULT NULL,
  `ili_desc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ili_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_line_items`
--

LOCK TABLES `invoice_line_items` WRITE;
/*!40000 ALTER TABLE `invoice_line_items` DISABLE KEYS */;
INSERT INTO `invoice_line_items` VALUES
(1,395.00,0.00,0.00,0.00,0.00,20,10001,3,'24\' Cattle Panel','with added welds'),
(2,90.00,0.00,0.00,10.00,10.00,34,10001,1,'Product #1',NULL);
/*!40000 ALTER TABLE `invoice_line_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_payments`
--

DROP TABLE IF EXISTS `invoice_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_payments` (
  `ip_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_amount` float(10,2) DEFAULT NULL,
  `ip_paymethod` varchar(20) DEFAULT NULL,
  `chkr_id` int(11) DEFAULT NULL,
  `btcr_id` int(11) DEFAULT NULL,
  `ip_notes` varchar(255) DEFAULT NULL,
  `i_id` int(11) DEFAULT NULL,
  `ip_dateadded` datetime DEFAULT NULL,
  PRIMARY KEY (`ip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_payments`
--

LOCK TABLES `invoice_payments` WRITE;
/*!40000 ALTER TABLE `invoice_payments` DISABLE KEYS */;
INSERT INTO `invoice_payments` VALUES
(1,1500.00,'Check',NULL,NULL,'Check #1234',10001,'2023-01-12 14:50:46');
/*!40000 ALTER TABLE `invoice_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_statuses`
--

DROP TABLE IF EXISTS `invoice_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_statuses` (
  `istat_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `istat_name` varchar(25) DEFAULT NULL,
  `istat_color` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`istat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_statuses`
--

LOCK TABLES `invoice_statuses` WRITE;
/*!40000 ALTER TABLE `invoice_statuses` DISABLE KEYS */;
INSERT INTO `invoice_statuses` VALUES
(1,'Draft','secondary'),
(2,'Paid','success'),
(3,'Sent','primary'),
(4,'Write Off','dark'),
(5,'Overdue','danger'),
(6,'Void','secondary');
/*!40000 ALTER TABLE `invoice_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `i_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_id` int(11) DEFAULT NULL,
  `i_amount` float(11,2) DEFAULT NULL,
  `i_discount` float(11,2) DEFAULT NULL,
  `i_discount_per` float(4,2) DEFAULT NULL,
  `i_tax` float(11,2) DEFAULT NULL,
  `i_tax_per` float(4,2) DEFAULT NULL,
  `istat_id` int(11) DEFAULT NULL,
  `i_paymethod` varchar(25) DEFAULT NULL,
  `i_terms` varchar(25) DEFAULT NULL,
  `i_date` datetime DEFAULT NULL,
  `i_duedate` datetime DEFAULT NULL,
  `i_notes_pub` text DEFAULT NULL,
  `i_notes_pri` text DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES
(10001,2,10960.00,548.00,5.00,253.14,6.25,3,'Card','Due on receipt','2022-12-14 15:02:36','2022-12-20 00:00:00',NULL,'These are the notes');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_statuses`
--

DROP TABLE IF EXISTS `job_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_statuses` (
  `js_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `js_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`js_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_statuses`
--

LOCK TABLES `job_statuses` WRITE;
/*!40000 ALTER TABLE `job_statuses` DISABLE KEYS */;
INSERT INTO `job_statuses` VALUES
(1,'Open'),
(2,'Upcoming'),
(3,'Completed'),
(4,'Cancelled'),
(5,'Late'),
(6,'Pending');
/*!40000 ALTER TABLE `job_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `j_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `j_name` varchar(50) DEFAULT NULL,
  `c_id` int(11) DEFAULT NULL,
  `j_dateadded` datetime DEFAULT NULL,
  `j_start` datetime DEFAULT NULL,
  `j_finish_goal` datetime DEFAULT NULL,
  `j_finish_true` datetime DEFAULT NULL,
  `js_id` int(11) DEFAULT NULL,
  `j_description` text DEFAULT NULL,
  `j_remind_date` datetime DEFAULT NULL,
  PRIMARY KEY (`j_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES
(1,'Job 1',2,'2022-09-02 14:36:26','2022-04-12 00:00:00','2022-04-17 00:00:00','2022-04-15 00:00:00',3,'This is what the job was about','2022-04-10 00:00:00'),
(2,'Corral Job',3,'2022-09-02 14:52:19','2022-09-12 00:00:00','2022-09-29 00:00:00',NULL,1,'This is what the job was about','2022-04-10 00:00:00');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_name` varchar(50) DEFAULT NULL,
  `p_sku` varchar(25) DEFAULT NULL,
  `p_type` varchar(7) DEFAULT NULL,
  `p_sell` int(1) DEFAULT NULL,
  `p_buy` int(1) DEFAULT NULL,
  `p_sellprice` float(11,2) DEFAULT NULL,
  `p_listprice` float(11,2) DEFAULT NULL,
  `p_wholesale` float(11,2) DEFAULT NULL,
  `tax_id` float(4,2) DEFAULT NULL,
  `wh_id` int(11) DEFAULT NULL,
  `pstat_id` int(11) DEFAULT NULL,
  `p_realqty` int(11) DEFAULT NULL,
  `p_desiredqty` int(11) DEFAULT NULL,
  `p_qtyalert` int(11) DEFAULT NULL,
  `p_length` float(6,2) DEFAULT NULL,
  `p_weight` float(10,2) DEFAULT NULL,
  `p_height` float(6,2) DEFAULT NULL,
  `p_width` float(6,2) DEFAULT NULL,
  `p_url` varchar(255) DEFAULT NULL,
  `p_desc` text DEFAULT NULL,
  `p_notes` text DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES
(1,'Product No. 1','PN1',NULL,1,0,100.00,90.00,70.00,1.00,1,1,1000,800,200,20.00,NULL,8.00,10.50,'https://erp.trinity.tel/example.html','Customer will see this.','Only we will see this.'),
(2,'Test Prod. #20','TP20',NULL,1,1,1000.00,850.75,650.00,1.00,1,2,1000,800,200,20.00,NULL,8.00,10.50,'https://erp.trinity.tel/example.html','Customer will see this.','Only we will see this.'),
(3,'24\' Cattle Panel','PANEL24REG',NULL,1,0,395.00,395.00,250.00,1.00,1,1,100,200,50,20.00,NULL,8.00,10.50,'https://erp.trinity.tel/example.html','Customer will see this.','Only we will see this.');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_codes`
--

DROP TABLE IF EXISTS `tax_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_codes` (
  `tc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tc_code` varchar(20) DEFAULT NULL,
  `tc_description` varchar(100) DEFAULT NULL,
  `tc_percent` float(4,2) DEFAULT NULL,
  PRIMARY KEY (`tc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_codes`
--

LOCK TABLES `tax_codes` WRITE;
/*!40000 ALTER TABLE `tax_codes` DISABLE KEYS */;
INSERT INTO `tax_codes` VALUES
(1,'NONE','No sales tax',0.00),
(2,'NAVARRO','Navarro County sales tax rate',6.25);
/*!40000 ALTER TABLE `tax_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_username` varchar(30) DEFAULT NULL,
  `u_fname` varchar(25) DEFAULT NULL,
  `u_lname` varchar(30) DEFAULT NULL,
  `u_email` varchar(50) DEFAULT NULL,
  `u_password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-17 10:32:14
