<?php
// Jobs

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'Products Database'    => 'products.php',
  'Add Product' => 'product-add.php',
);
subnav($subnav);

open_content();

echo page_title("Product X31");
nav_active('products');

?>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3">
    <div class="input-group">
      <a role="button" class="btn btn-sm btn-outline-secondary" href="products.php">Back to Products</a>
      <a role="button" class="btn btn-sm btn-outline-secondary" href="product-edit.php?p_id=1">Edit Product</a>
    </div>
  </div>
  <div class="row mb-3"><!-- Main container row -->
    <!-- Information -->
    <div class="col-md-8">
      <div class="row mb-3">
          <table class="table table-striped w-100">
            <thead>
              <tr>
                <th scope="col" colspan="2">Product Information</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Status</td>
                <td><span class="badge bg-success">Visible</span></td>
              </tr>
              <tr>
                <td>Category</td>
                <td><a href="product-category-view.php?pcat_id=1">Ranching Equipment</a></td>
              </tr>
              <tr>
                <td>Price</td>
                <td>$400.00</td>
              </tr>
              <tr>
                <td>List Price</td>
                <td>$240.00</td>
              </tr>
              <tr>
                <td>Wholesale Price</td>
                <td>$330.00</td>
              </tr>
              <tr>
                <td>Weight</td>
                <td>500 lb.</td>
              </tr>
              <tr>
                <td>Quantity</td>
                <td><span class="badge bg-danger">Out of Stock</span></td>
              </tr>
              <tr>
                <td>Vendor</td>
                <td><a href="vendor-view.php?v_id=1">Maker, Co.</a></td>
              </tr>
              <tr>
                <td colspan="2"><strong>Description</strong><br />
                  This is the description.<br />
                  1. This is an item<br>
                  2. This is another item<br>
                  Might finish late!
                </td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>

    <!-- Pictures -->
    <div class="col-md-4">
      <div class="row mb-3">
        <div class="col">
          <img class="img-thumbnail w-100" src="images/blank.png" alt="..." />
        </div>
      </div>
    </div>

  </div>
</div>



<?php

close_content();

// Get footer
require('assets/footer.php');

?>
