<?php
// Main file

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'View Employee'    => 'customers.php',
  'Add Employee' => 'customer-add.php',
  'View Payrolls' => 'payroll.php',
  'Add Paystub' => 'payroll-paystub.php'
);
subnav($subnav);

open_content();

echo page_title("Employees");
nav_active('hr');

?>

<div class="container" id="containerWidth">
  <div class="row mb-3">
    <div class="col-md-4">
      <input type="text" class="form-control form-control-sm" id="customerSearch" placeholder="Search Employees (ID, last/first name)" autofocus>
    </div>
    <div class="col-md-4">
      <select class="form-control form-control-sm">
        <option selected disabled>By Type</option>
      </select>
    </div>
    <div class="col-md-4 d-flex">
      <a href="customer-add.php" class="btn btn-sm btn-outline-secondary ms-auto"><i class="bi-person"></i> New Employee</a>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col"><a class="text-dark" href="customers.php?order_by=c_id&asc=true" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by ID"># <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col"><a class="text-dark" href="customers.php?order_by=c_lname&asc=true" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by last name">Last, First <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col"><a class="text-dark" href="customers.php?order_by=c_cphone&asc=true" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by cell phone">Cell Phone <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col">Jobs Open</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="col">1</th>
            <td><a href="customer-view.php?c_id=1">Doe, John</a></td>
            <td>1234567890</td>
            <td><a href="customer-jobs.php?c_id=1">2</a></td>
          </tr>
          <tr>
            <td colspan="4">No results</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <!-- Pagination -->
  <div class="row">
    <div class="col-md-6">
      <nav aria-label="...">
        <ul class="pagination">
          <li class="page-item disabled">
            <span class="page-link"><i class="bi-arrow-left-circle"></i></span>
          </li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item active" aria-current="page">
            <span class="page-link">2</span>
          </li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#"><i class="bi-arrow-right-circle"></i></a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
