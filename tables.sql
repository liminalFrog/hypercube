CREATE TABLE company_settings (
    company_name varchar(30),
    company_address1 varchar(50),
    company_address2 varchar(50),
    company_city varchar(25),
    company_state char(2),
    company_zip varchar(20),
    company_email varchar(35),
    company_phone varchar(15),
    company_fax varchar(15),
    company_ein varchar(15)
);

DROP TABLE tax_codes;

CREATE TABLE tax_codes (
    tc_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    tc_name varchar(20),
    tc_desc varchar(100),
    tc_per float(4,2),
    tc_datecreated datetime
);

INSERT INTO tax_codes (tc_name, tc_desc, tc_per, tc_datecreated) VALUES ("SS", "Social Security Tax", 6.20, NOW()), ("MEDICARE" "Medicare Tax", 1.45, NOW());

CREATE TABLE users (
    user_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_username varchar(25),
    user_password varchar(255),
    user_dateadded datetime,
    user_status_id int,
    user_lname varchar(25),
    user_fname varchar(25),
    employee_id int,
    user_type varchar(15),
    user_profile_pic varchar(100)
);

CREATE TABLE members (
    m_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    m_fname varchar(25),
    m_initial char(1),
    m_lname varchar(25)
);

CREATE TABLE employees (
    emp_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    emp_fname varchar(25),
    emp_initial char(1),
    emp_lname varchar(25),
    emp_hiredate date,
    emp_datecreated datetime,
    addr_id int
);

CREATE TABLE positions (
    po_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    po_name varchar(35),
    po_desc text,
    po_datecreated datetime
);

CREATE TABLE employee_positions (
    ep_int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    emp_id int,
    po_id int,
    ep_datecreated datetime,
    ep_dateexpire datetime
);

CREATE TABLE payrolls (
    pr_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    emp_id int,
    pr_type varchar(10), /* salary, wage, commission, bonus */
    pr_units int unsigned, /* hours/days, units sold, or just 1 */
    pr_priceperunit float(10,2),
    tax_id int, /* withheld percentage */
    pr_amount float(10,2) /* total amount paid out */
);

CREATE TABLE paystubs (
    ps_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ps_name varchar(35),
    ps_notes text,
    pr_id int,
    pr_amount float(10,2),
    pr_date datetime
);

CREATE TABLE checks_cashed (
    check_cash_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    check_cash_num int,
    check_cash_datecashed datetime,
    customer_id int,
    vendor_id int,
    check_cash_notes text
)

CREATE TABLE checks_written (
    check_write_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    check_write_num int,
    check_write_datecashed datetime,
    customer_id int,
    vendor_id int,
    check_write_notes text
)
