<?php

// Log out administrator
session_start();

if(isset($_SESSION['user'])){
  unset($_SESSION['user']);
}

// redirect
header('location: ../login.php?logout=true');
exit();

?>
