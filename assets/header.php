<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Hypercube</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/offcanvas-navbar/">



    <!-- Bootstrap core CSS -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <!-- Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/5.1/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
<link rel="icon" href="/docs/5.1/assets/img/favicons/favicon.ico">
<meta name="theme-color" content="#7952b3">


    <style>
      @font-face {
          font-family: 'alegreyaregular';
          src: url('css/alegreya-regular-webfont.woff2') format('woff2'),
              url('css/alegreya-regular-webfont.woff') format('woff');
          font-weight: normal;
          font-style: normal;
      }
      @font-face {
          font-family: 'playfairdisplay_regular';
          src: url('css/playfairdisplay-regular-webfont.woff2') format('woff2'),
              url('css/playfairdisplay-regular-webfont.woff') format('woff');
          font-weight: normal;
          font-style: normal;
      }
      body {
        font-family: 'Noto Serif CJK HK', 'Noto Sans Mono';
        font-weight: medium;
      }
      .btn, input[type=submit], .badge {
        box-shadow:inset 0px 1px 0px 0px rgba(255,255,255,0.5);
      	background-image:var(--bs-gradient);
      	border-radius:5px;
      	border:1px solid inherit;
        border-bottom-color: rgba(0,0,0,0.5);
      	display:inline-block;
      	text-decoration:none
      }
      .btn-outline-primary,.btn-outline-secondary,.btn-outline-danger,.btn-outline-success,.btn-outline-warning,.btn-outline-info,.btn-outline-dark {
        box-shadow:inset 0px 1px 0px 0px rgba(255,255,255,0.5);
        background-color: rgba(255,255,255,0.6);
      	background-image:var(--bs-gradient);
      }
      .btn-outline-primary:hover,.btn-outline-secondary:hover,.btn-outline-danger:hover,.btn-outline-success:hover,.btn-outline-warning:hover,.btn-outline-info:hover,.btn-outline-dark:hover {

      }
      .btn:active{
        margin-top:2px;
        margin-bottom:-2px;
      }
      .btn-outline-primary:focus,.btn-outline-secondary:focus,.btn-outline-danger:focus,.btn-outline-success:focus,.btn-outline-warning:focus,.btn-outline-info:focus,.btn-outline-dark:focus {

      }

      h1,h2,h3,h4,h5,h6 {
        font-family: 'playfairdisplay_regular'
      }
      .form-control, .input-group-text, .form-select {
        border-color:#aaa
      }
      .form-control, .input-group-text, .form-select, .alert {
        box-shadow: 1px 1px 3px #ccc
      }
      .badge, .ui-datepicker {
        font-family: 'Noto Sans Mono' !important
      }
      .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br,
      .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl,
      .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr,
      .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
        border-radius: 0.7em !important
      }

    </style>


    <!-- Custom styles for this template -->
    <link href="css/offcanvas.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <!-- jQuery scripts -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
  </head>
  <body class="bg-white pe-0">

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark py-1 pe-0 d-print-none" style="text-shadow:0 1px 1px #000" aria-label="Main navigation">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">
      <img src="images/logo.png" height="40" title="Litany" alt="Logo" />
    </a>

<?php
  // Show navbar if admin is logged in
  if(!$_SESSION['admin']){
?>

    <button class="navbar-toggler p-0 border-0" type="button" id="navbarSideCollapse" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a id="dashboard" class="nav-link" aria-current="page" href="index.php"><i class="bi-speedometer"></i>&nbsp; Dashboard</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="crm" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi-people"></i>&nbsp; CRM</a>
          <ul class="dropdown-menu dropdown-menu-dark shadow" aria-labelledby="crm">
            <li><a class="dropdown-item" href="customers.php">Customers</a></li>
            <li><a class="dropdown-item" href="jobs.php">Jobs</a></li>
            <li><a class="dropdown-item" href="events.php">Customer Events</a></li>
            <li><a class="dropdown-item" href="invoices.php">Invoices</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a id="products" class="nav-link" href="products.php"><i class="bi-box-seam"></i>&nbsp; Products</a>
        </li>
        <li class="nav-item">
          <a id="vendors" class="nav-link" href="vendors.php"><i class="bi-truck"></i>&nbsp; Vendors</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="hr" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi-person-badge"></i>&nbsp; HR</a>
          <ul class="dropdown-menu dropdown-menu-dark shadow" aria-labelledby="hr">
            <li><a class="dropdown-item" href="employees.php">Employees</a></li>
            <li><a class="dropdown-item" href="payroll.php">Payroll</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="hr" data-bs-toggle="dropdown" aria-expanded="false"><i class="bi-graph-up-arrow"></i>&nbsp; Finance</a>
          <ul class="dropdown-menu dropdown-menu-dark shadow" aria-labelledby="hr">
            <li><a class="dropdown-item" href="employees.php">Statements</a></li>
            <li><a class="dropdown-item" href="payroll.php">Bills</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a id="settings" class="nav-link" href="settings.php"><i class="bi-gear-wide-connected"></i>&nbsp; Settings</a>
        </li>
        <li class="nav-item">
          <a id="logout" class="nav-link" href="assets/logout.php"><i class="bi-door-open"></i>&nbsp; Log Out</a>
        </li>
      </ul>
      <!-- <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form> -->
    </div>
  <?php
  } // end if logged in

  // Highlight active page
  nav_active($active_link);
  ?>
  </div>
</nav>
