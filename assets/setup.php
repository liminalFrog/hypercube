<?php
// error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();

require('functions.php');

$url = "location: ../setup.php";

// Check if user is starting from /public_html/setup.php
if (!$_SESSION['setup']['step'] || $_SESSION['setup']['step'] == ""){
    header("location: ../setup.php");
    die();
} else {
    // Begin installation
    if (sec($_GET['clear'])) {
        $_SESSION['setup']['step'] = 'x';
    }

    $step = $_SESSION['setup']['step'];

    // Unset error triggers
    unset($_SESSION['setup']['err']);
    $err = false;

    // Which step?
    switch($step){
        case 1:

            // Redirect to case 2 and begin setup
            $_SESSION['setup']['step'] = 2;
            header($url);
            die();

        case 2:

            // Check for empty fields
            if(!$_POST['hostname'] || $_POST['hostname'] == ""){
                $err = true;
                $_SESSION['setup']['err']['hostname_empty'] = 1;
            } else {
                if (
                    !$_SESSION['setup']['hostname'] ||
                    ($_SESSION['setup']['hostname'] != sec($_POST['hostname']))
                ) {
                    $_SESSION['setup']['hostname'] = sec($_POST['hostname']);
                }
            }
            if(!$_POST['username'] || $_POST['username'] == ""){
                $err = true;
                $_SESSION['setup']['err']['username_empty'] = 1;
            } else {
                if (
                    !$_SESSION['setup']['username'] ||
                    ($_SESSION['setup']['username'] != sec($_POST['username']))
                ) {
                    $_SESSION['setup']['username'] = sec($_POST['username']);
                }
            }
            if(!$_POST['password'] || $_POST['password'] == ""){
                $err = true;
                $_SESSION['setup']['err']['password_empty'] = 1;
            } else {
                if (
                    !$_SESSION['setup']['password'] ||
                    ($_SESSION['setup']['password'] != sec($_POST['password']))
                ) {
                    $_SESSION['setup']['password'] = sec($_POST['password']);
                }
            }
            if(!$_POST['database'] || $_POST['database'] == ""){
                $err = true;
                $_SESSION['setup']['err']['database_empty'] = 1;
            } else {
                if (
                    !$_SESSION['setup']['database'] ||
                    ($_SESSION['setup']['database'] != sec($_POST['database']))
                ) {
                    $_SESSION['setup']['database'] = sec($_POST['database']);
                }
            }

            // Check for invalid characters
            if (invalid_strict($_POST['hostname'])) {
                $err = true;
                $_SESSION['setup']['err']['hostname_invalid'] = 1;
            }
            if (invalid_strict($_POST['username'])) {
                $err = true;
                $_SESSION['setup']['err']['username_invalid'] = 1;
            }
            if (invalid_strict($_POST['password'])) {
                $err = true;
                $_SESSION['setup']['err']['password_invalid'] = 1;
            }
            if (invalid_strict($_POST['database'])) {
                $err = true;
                $_SESSION['setup']['err']['database_invalid'] = 1;
            }
            
            // If errors were triggered
            if ($err) {
                header($url);
                die();
            }

            // Put POST into variables
            $hostname = sec($_POST['hostname']);
            $username = sec($_POST['username']);
            $password = sec($_POST['password']);
            $database = sec($_POST['database']);

            // Check database connection          
            try {
                mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
                $db = new mysqli($hostname, $username, $password, $database);
            } catch (mysqli_sql_exception $e) {
                $_SESSION['setup']['err']['connect_error'] = 1;
                header($url);
                die();
            }
            unset($_SESSION['setup']['err']['connect_error']);

            // Success. Create DB config folder and file
            $dir = '../../';
            $folder = $dir . 'hypercube_config/';
            if(is_dir($dir)){
              if(is_writable($dir)){
                if (!file_exists($folder)) {
                    if(!mkdir($folder, 0777)){
                        $err;
                        $_SESSION['setup']['err']['nowrite'] = 1;
                    }
                }
                // Create file
                $filename = 'db';
                if (file_exists($folder)){
                    $file = fopen($folder.$filename, 'w');
                    $txt = "h=$hostname&u=$username&p=$password&d=$database";
                    fwrite($file, $txt);
                    fclose($file);
                    // Check file
                    if (is_readable($folder.$filename)) {
                        $chk_file = fopen($folder.$filename, 'rb');
                        $text = fread($chk_file, filesize($folder.$filename));
                        fclose($chk_file);
                        parse_str($text, $output);
                        // Try database with file
                        try {
                            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
                            $db = new mysqli($output['h'], $output['u'], $output['p'], $output['d']);
                        } catch (mysqli_sql_exception $e) {
                            $err;
                            $_SESSION['setup']['err']['config_connect'] = 1;
                        }
                    } else {
                        $_SESSION['setup']['err']['config_connect'] = 1;
                    }
                } else {
                    $err;
                    $_SESSION['setup']['err']['nowrite'] = 1;
                }

            } else {
                $err;
                $_SESSION['setup']['err']['nowrite'] = 1;
              }
            }

            // If no errors were triggered
            if (!$err) {
                unset($_SESSION['setup']['err']);
                $_SESSION['setup']['step'] = 3;
            }
            // Finish
            header($url);
            die();
            
        break;

        case 3:
            echo '<pre>';
            print_r($_POST);
            echo '</pre>';
            // Check if company name empty (others not required)
            if (!$_POST['company_name'] || $_POST['company_name'] == "") {
                $err = true;
                $_SESSION['setup']['err']['company_name_empty'] = 1;
            }

            // Check for invalid characters
            if ($_POST['company_name'] && $_POST['company_name'] != "") {
                if (invalid($_POST['company_name'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_name_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_name'] ||
                        ($_SESSION['setup']['company_name'] != $_POST['company_name'])
                    ) {
                        $_SESSION['setup']['company_name'] = $_POST['company_name'];
                    }
                }
            }
            if ($_POST['company_address1'] && $_POST['company_address1'] != "") {
                if (invalid($_POST['company_address1'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_address1_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_address1'] ||
                        ($_SESSION['setup']['company_address1'] != $_POST['company_address1'])
                    ) {
                        $_SESSION['setup']['company_address1'] = $_POST['company_address1'];
                    }
                }
            }
            if ($_POST['company_address2'] && $_POST['company_address2'] != "") {
                if (invalid($_POST['company_address2'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_address2_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_address2'] ||
                        ($_SESSION['setup']['company_address2'] != $_POST['company_address2'])
                    ) {
                        $_SESSION['setup']['company_address2'] = $_POST['company_address2'];
                    }
                }
            }
            if ($_POST['company_city'] && $_POST['company_city'] != "") {
                if (invalid($_POST['company_city'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_city_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_city'] ||
                        ($_SESSION['setup']['company_city'] != $_POST['company_city'])
                    ) {
                        $_SESSION['setup']['company_city'] = $_POST['company_city'];
                    }
                }
            }
            if ($_POST['company_state'] && $_POST['company_state'] != "") {
                if (state_invalid($_POST['company_state'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_state_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_state'] ||
                        ($_SESSION['setup']['company_state'] != $_POST['company_state'])
                    ) {
                        $_SESSION['setup']['company_state'] = $_POST['company_state'];
                    }
                }
            }
            if ($_POST['company_zip'] && $_POST['company_zip'] != "") {
                if (zip_invalid($_POST['company_zip'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_zip_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_zip'] ||
                        ($_SESSION['setup']['company_zip'] != $_POST['company_zip'])
                    ) {
                        $_SESSION['setup']['company_zip'] = $_POST['company_zip'];
                    }
                }
            }
            if ($_POST['company_phone'] && $_POST['company_phone'] != "") {
                if (phone_invalid($_POST['company_phone'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_phone_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_phone'] ||
                        ($_SESSION['setup']['company_phone'] != $_POST['company_phone'])
                    ) {
                        $_SESSION['setup']['company_phone'] = $_POST['company_phone'];
                    }
                }
            }
            if ($_POST['company_fax'] && $_POST['company_fax'] != "") {
                if (phone_invalid($_POST['company_fax'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_fax_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_fax'] ||
                        ($_SESSION['setup']['company_fax'] != $_POST['company_fax'])
                    ) {
                        $_SESSION['setup']['company_fax'] = $_POST['company_fax'];
                    }
                }
            }
            if ($_POST['company_email'] && $_POST['company_email'] != "") {
                if (email_invalid($_POST['company_email'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_email_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_email'] ||
                        ($_SESSION['setup']['company_email'] != $_POST['company_email'])
                    ) {
                        $_SESSION['setup']['company_email'] = $_POST['company_email'];
                    }
                }
            }

            if ($_POST['company_ein'] && $_POST['company_ein'] != "") {
                if (ein_invalid($_POST['company_ein'])) {
                    $err = true;
                    $_SESSION['setup']['err']['company_ein_invalid'] = 1;
                } else {
                    if (
                        !$_SESSION['setup']['company_ein'] ||
                        ($_SESSION['setup']['company_ein'] != $_POST['company_ein'])
                    ) {
                        $_SESSION['setup']['company_ein'] = $_POST['company_ein'];
                    }
                }
            }
            
            // If errors, go back
            if ($err) {
                header($url);
                die();
            }

            // Insert into company settings
            
        break;

        // Clear sessions/start over
        default:
            unset($_SESSION['setup']);
            header($url);
            die();
        break;
    }
}

?>