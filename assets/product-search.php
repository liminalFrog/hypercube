<?php

// Search product database with query

require('database.php');

$q = $_POST['productSearch'];

if ($q != "" && isset($q)) {
  $command = 'SELECT p_id, p_name, p_price, p_list_price, p_wholesale_price FROM products HAVING p_name LIKE "%'.$q.'%" ORDER BY p_name ASC LIMIT 5;';

  $result = $db->query($command);

  $i = 1;
  while ($data = $result->fetch_array()) {
    if ($i == 1){ // First listing (press Enter to select Product)
      echo '<a href="#" class="list-group-item list-group-item-action list-group-item-primary" onclick="selectExistingProduct('.$data['p_id'].', \''.$data['p_name'].'\', '.$data['p_price'].', '.$data['p_wholesale_price'].', '.$data['p_list_price'].')">
      <input type="hidden" value="'.$data['p_id'].'" id="highlightedProductId" />
      <input type="hidden" value="'.$data['p_name'].'" id="highlightedProductName" />
      '.$data['p_name'].'</a>';
      $i++;
    } else { // Normal listing
      echo '<a href="#" class="list-group-item list-group-item-action" onclick="selectExistingProduct('.$data['p_id'].', \''.$data['p_name'].'\', '.$data['p_price'].', '.$data['p_wholesale_price'].', '.$data['p_list_price'].')">'.$data['p_name'].'</a>';
    }
  }
} else {
  echo 'hi';
}

?>
