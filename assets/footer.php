<?php

// Footer

 ?>

<div class="container d-print-none">
  <footer class="row row-cols-2 py-5 my-5 border-top">
    <div class="col fs-6 text-start">
      <p class="badge text-start text-muted p-0">Hypercube ERP &copy; <?php echo date('Y'); ?></p>
    </div>

    <div class="col text-end">
      <p class="badge text-muted p-0"><a href="about.php">About Hypercube</a></p>
    </div>
  </footer>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>
