<?php

// Functions for Hypercube

// Bootstrap Functions

// Tags
function p($content, $styles = null){
  return $output = '<p class="'.$styles.'">'.$content.'</p>';
}

function container($content){
  return $output = '<div class="container">'.$content.'</div>';
}
function container_fluid($content){
  return $output = '<div class="container-fluid">'.$content.'</div>';
}

function row($content, $margin_bottom = 3, $styles = null){
  return $output = '<div class="row mb-'.$margin_bottom.' '.$styles.'">'.$content.'</div>';
}

function col($content, $width = null, $styles = null, $id = null){
  return $output = '<div class="col col-'.$width.' '.$styles.'" id="'.$id.'">'.$content.'</div>';
}

// GET values in URL field (CAREFUL WHAT YOU PASS IN HERE)
if (isset($_GET) && $_GET != 0) {
        foreach ($_GET as $param_name => $param_val) {
                $$param_name = $param_val;
        }
}

// POST values (CAREFUL WHAT YOU PASS IN HERE)
if (isset($_POST) && $_POST != 0) {
        foreach ($_POST as $param_name => $param_val) {
                $$param_name = $param_val;
        }
}
// REGEX FUNCTIONS
// Check for invalid characters
function invalid_strict($str) {
  if (preg_match("/^[A-Za-z0-9.#\-&\*\$`~@,_]+$/", $str)) {
      return false; // No good
  } else {
      return true; // All good
  }
}
function invalid($str) {
  if (preg_match("/^[A-Za-z0-9.#\-&\*'\$`~@,_ ]+$/", $str)) {
      return false; // No good
  } else {
      return true; // All good
  }
}

function email_invalid($str) {
  $regex = '/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
  if (preg_match($regex, $str)) {
    return false;
  } else {
    return true;
  }
}

function phone_invalid($str) {
  $regex = '/^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$/';
  if (preg_match($regex, $str)) {
    return false;
  } else {
    return true;
  }
}

function zip_invalid($str) {
  $regex = '/^\d{5}(?:[-\s]\d{4})?$/';
  if (preg_match($regex, $str)) {
    return false;
  } else {
    return true;
  }
}

function states_array() {
  $states = array(
    "AL" => "Alabama",
    "AK" => "Alaska",
    "AZ" => "Arizona",
    "AR" => "Arkansas",
    "CA" => "California",
    "CO" => "Colorado",
    "CT" => "Connecticut",
    "DE" => "Delaware",
    "DC" => "District of Columbia",
    "FL" => "Florida",
    "GA" => "Georgia",
    "HI" => "Hawaii",
    "ID" => "Idaho",
    "IL" => "Illinois",
    "IN" => "Indiana",
    "IA" => "Iowa",
    "KS" => "Kansas",
    "KY" => "Kentucky",
    "LA" => "Louisiana",
    "ME" => "Maine",
    "MD" => "Maryland",
    "MA" => "Massachusetts",
    "MI" => "Michigan",
    "MN" => "Minnesota",
    "MS" => "Mississippi",
    "MO" => "Missouri",
    "MT" => "Montana",
    "NE" => "Nebraska",
    "NV" => "Nevada",
    "NH" => "New Hampshire",
    "NJ" => "New Jersey",
    "NM" => "New Mexico",
    "NY" => "New York",
    "NC" => "North Carolina",
    "ND" => "North Dakota",
    "OH" => "Ohio",
    "OK" => "Oklahoma",
    "OR" => "Oregon",
    "PA" => "Pennsylvania",
    "RI" => "Rhode Island",
    "SC" => "South Carolina",
    "SD" => "South Dakota",
    "TN" => "Tennessee",
    "TX" => "Texas",
    "UT" => "Utah",
    "VT" => "Vermont",
    "VA" => "Virginia",
    "WA" => "Washington",
    "WV" => "West Virginia",
    "WI" => "Wisconsin",
    "WY" => "Wyoming"
  );
  return $states;
}

function state_invalid($str) {
  if ( array_key_exists( $str, states_array() ) ) {
    return false;
  } else {
    return true;
  }
}

function ein_invalid($str) {
  $regex = '/^\d{2}\-?\d{7}$/';
  if (preg_match($regex, $str)) {
    return false;
  } else {
    return true;
  }
}

// Quick HTML cleanup for security
function sec($str) {
  return stripslashes(htmlentities($str));
}

// Opening and closing page content
function open_content(){
  echo '<main class="container-fluid"><div class="container-fluid py-3">';
}
function close_content(){
  echo '</div></main>';
}

// Add page name and change title in tab
function page_title($title){
  $header = '<h2 class="py-3 d-print-none">'.$title.'</h2>';
  // change title
  echo '<script>
    document.title = "'.$title.'";
  </script>';
  return $header; // optional print out
}

// Create navbar
function nav_active($link){
  echo '<script>document.getElementById("'.$link.'").classList.add("active");</script>';
}

// Create sub-navbar
function subnav($links_array){
  $subnav = '<div class="nav-scroller border-bottom d-print-none">
    <nav class="nav nav-underline" aria-label="Secondary navigation">
      ';
      foreach ($links_array as $name => $link){
        if(!$link || $link == ''){
          $subnav .= '<span class="nav-link disabled">'.$name.'</span>';
        } else {
          $subnav .= '<a class="nav-link text-decoration-underline';
          // Current page
          if(basename($_SERVER['PHP_SELF']) == $link)
            $subnav .= ' active text-primary';
          $subnav .= '" href="'.$link.'">'.$name.'</a>';
        }
      }
      $subnav .= '
      <!--<a class="nav-link" href="assets/logout.php"><i class="bi-door-open"></i>&nbsp; Log Out</a>-->
    </nav>
  </div>';
  echo $subnav;
}

function validation(){
  require('assets/form-validation.php');
}

// ALERTS
function alert_success($msg){
  echo '<div class="alert alert-success">'.$msg.'</div>';
}
function alert_primary($msg){
  echo '<div class="alert alert-primary">'.$msg.'</div>';
}
function alert_danger($msg){
  echo '<div class="alert alert-danger">'.$msg.'</div>';
}
function alert_info($msg){
  echo '<div class="alert alert-info">'.$msg.'</div>';
}
function alert_warning($msg){
  echo '<div class="alert alert-warning">'.$msg.'</div>';
}
function alert_dark($msg){
  echo '<div class="alert alert-dark">'.$msg.'</div>';
}

// TEXT COLOR MESSAGES
function text_success($msg){
  echo '<span class="text-success">'.$msg.'</span>';
}
function text_primary($msg){
  echo '<span class="text-primary">'.$msg.'</span>';
}
function text_danger($msg){
  echo '<span class="text-danger">'.$msg.'</span>';
}
function text_info($msg){
  echo '<span class="text-info">'.$msg.'</span>';
}
function text_warning($msg){
  echo '<span class="text-warning">'.$msg.'</span>';
}
function text_dark($msg){
  echo '<span class="text-dark">'.$msg.'</span>';
}

function user_register($u_fname, $u_lname, $u_username, $u_password, $u_email){
  	 	global $db;
  	 	$u_fname = mysqli_real_escape_string($db, $u_fname);
      $u_lname = mysqli_real_escape_string($db, $u_lname);
   	 	$u_username = mysqli_real_escape_string($db, $u_username);
   	 	$u_password = mysqli_real_escape_string($db, $u_password);
   	 	$u_email = mysqli_real_escape_string($db, $u_email);
  	 	$wp_hasher = new PasswordHash(16, true);
 	 	  $u_password = $wp_hasher->HashPassword( trim( $u_password ) );

  	 	$sql = "INSERT INTO users (u_fname, u_lname u_email, u_password, u_username) VALUES ('".$u_fname."', '".$u_lname."', '".$u_email."', '".$u_password."', '".$u_username."') ";
  	 	$result = mysqli_query($db, $sql);
  	 	return mysqli_insert_id($db);
 	}

function user_login($user, $pass){
  	global $db;
  	$username = mysqli_real_escape_string($db, $user);
   	$password = mysqli_real_escape_string($db, $pass);
  	$sql = "SELECT id, password FROM kv_users WHERE username='".$username."' LIMIT 1 ";
  	$result = mysqli_query($db, $sql);
  	$id = mysqli_fetch_row($result);
    if($id){
  	 	$password_hashed = $id[1];
 	 	 $wp_hasher = new PasswordHash(16, true);
 	 	 if($wp_hasher->CheckPassword($password, $password_hashed)) {
  	 	 	return $id[0];
  	 	}
  	} else {
  	 	return false;
  	}
}

// Get data and put into array
function getdata($sql){
  global $db;
  $result = $db->query($sql);
  while($data = $result->fetch_array()){
    return $data;
  }
}

// Format number as currency
function currency($var){
  return sprintf("%.2f", (float)$var);
}

// Bitcoin symbol
function btc_symbol(){
  return $var = '<i class="bi-currency-bitcoin"></i>';
}

// Format currency with commas (for displaying a final number, not for math)
function commas($var){
  $regex = "/\B(?=(\d{3})+(?!\d))/i";
  $amount = preg_replace($regex, ",", $var);
  return $amount;
}

// Fill with zero if empty
function fill_zero($var) {
  if($var == "" || is_null($var)){
    return (int)$var = "0";
  } else {
    return (float)$var;
  }
}

// Get any field (f) info from logged in user
function user($f){
  return $_SESSION['user'][$f];
}

// Get customer's full name by ID
function c_fullname($id) {
  global $db;
  $sql = 'SELECT CONCAT(c_fname, " ", c_lname) AS name FROM customers WHERE c_id = '.$id.' LIMIT 1;';
  $result = $db->query($sql);
  $fullname = mysqli_fetch_row($result);
  return $fullname[0];
}

function c_address($id) {
  global $db;
  $sql = 'SELECT c_address1, c_address2, c_city, c_state, c_zip FROM customers WHERE c_id = '.$id.' LIMIT 1;';
  $result = $db->query($sql);
  $address = mysqli_fetch_row($result);
  return $address;
}

?>
