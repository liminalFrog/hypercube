<?php



// Database Information

// error_reporting(E_ALL);
// ini_set('display_errors', '1');


// Get database information
$filename = '../hypercube_config/db';

if(file_exists($filename)){
  if(is_readable($filename)){
    // Read contents
    $db_file = fopen($filename, 'rb');
    $db_text = fread($db_file, filesize($filename));
    fclose($db_file);
    parse_str($db_text, $output);
  } else {
    // Unreadable
    echo 'Error: It seems that the Hypercube database file is unreadable. Check permissions in the file:<br><pre>web_root/hypercube_config/db</pre>';
  }
} else {
  // Hypercube is not set up or file has been deleted.
  unset($_SESSION['setup']);
  header('location: setup.php');
  die();
}

$db_server    = $output['h'];
$db_username  = $output['u'];
$db_password  = $output['p'];
$db_database  = $output['d'];

// New Connection
$db = new mysqli($db_server, $db_username, $db_password, $db_database);

// Check connection
if ($db->connect_error) {
  die("Connection failed: " . $db->connect_error);
}

?> 
