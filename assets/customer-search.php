<?php

// Search customer database with query

require('database.php');

$q = $_POST['customerSearch'];

if ($q != "" && isset($q)) {
  $command = 'SELECT c_id, CONCAT(c_fname, " ", c_lname) AS c_name FROM customers HAVING c_name LIKE "%'.$q.'%" ORDER BY c_name ASC LIMIT 5;';

  $result = $db->query($command);

  $i = 1;
  while ($data = $result->fetch_array()) {
    if ($i == 1){ // First listing (press Enter to select customer)
      echo '<a href="#" class="list-group-item list-group-item-action list-group-item-primary" href="#" onclick="selectExistingCustomer('.$data['c_id'].', \''.$data['c_name'].'\')">
      <input type="hidden" id="highlightedCustomerId" value="'.$data['c_id'].'" />
      <input type="hidden" id="highlightedCustomerName" value="'.$data['c_name'].'" />
      '.$data['c_name'].'</a>';
      $i++;
    } else { // Normal listing
      echo '<a class="list-group-item list-group-item-action" href="#" onclick="selectExistingCustomer('.$data['c_id'].', \''.$data['c_name'].'\')">'.$data['c_name'].'</a>';
    }
  }
} else {
  echo '';
}

?>
