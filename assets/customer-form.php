<?php
  // Customer form

  if ( (!$c_id || $c_id == '') && (isset($_SESSION['user']['add_invoice']['c_id']) && $_SESSION['user']['add_invoice']['c_id'] != '') ) {
    $c_id = $_SESSION['user']['add_invoice']['c_id'];
  }
?>
<!-- Customer Info -->
<div class="col-md-5 mb-3">
  <label for="pills-tab" class="form-label">Customer Info</label>
  <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
    <li class="nav-item" role="presentation">
      <button class="nav-link active" id="existingCustomer-tab" data-bs-toggle="pill" data-bs-target="#existingCustomer" type="button" role="tab" aria-controls="existingCustomer" aria-selected="true" value="exists" onclick="customerOptionSwitch(this.value)">Existing Customer</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="newCustomerForm-tab" data-bs-toggle="pill" data-bs-target="#newCustomerForm" type="button" role="tab" aria-controls="newCustomerForm" aria-selected="false" value="new" onclick="customerOptionSwitch(this.value)">New Customer</button>
    </li>
  </ul>
  <div class="tab-content" id="pills-tabContent">
    <!-- Existing Customer -->
    <div class="tab-pane fade show active" id="existingCustomer" role="tabpanel" aria-labelledby="existingCustomer-tab">
      <div id="searchCustomerDiv" class="form-control" <?php
      if ($c_id && $c_id != '')
        echo 'style="display: none;"';
      ?>>
        <div class="row mb-1">
          <div class="col-12">
            <label for="c_search" class="form-label">Search Customers: <a href="#!" id="searchCancel" class="badge link-danger" style="display:<?php if ($c_id && $c_id != '') { echo 'content'; } else {echo 'none';} ?>" onclick="resetCustomerCancel()">Cancel</a></label>
            <input name="customerSearch" id="customerSearch" type="text" class="form-control" placeholder="First/last name" autocomplete="off">
            <p id="text"></p>
          </div>
        </div>
        <div class="row mb-1">
          <div class="col-12">
            <div class="list-group shadow-sm" id="customerList">
              <!-- Customer List goes here -->
            </div>
          </div>
        </div>
      </div>
      <?php
        // IF CUSTOMER SELECTED IN SESSION


      ?>
      <div class="row mb-1" id="selectedCustomerDiv" <?php if ($c_id && $c_id != ''){ echo 'style="display: block"'; } else {echo 'style="display: none"';} ?>>
        <div class="col-12 form-control">
          <label for="selectedCustomerP" class="form-label mb-0"><small>Selected Customer</small></label>
          <p id="selectedCustomerP" class="mb-0"><span class="text-success" id="selectedCustomerName"><?php if ($c_id && $c_id != ''){ echo c_fullname($c_id); } ?></span> <a href="#!" class="badge link-primary" onclick="resetCustomer()">Change</a></p>
          <input type="hidden" name="c_id" id="selectedCustomerId" value="<?php if ($c_id && $c_id != ''){ echo $c_id; } ?>" />
        </div>
      </div>

    </div>

    <!-- Create New Customer -->
    <div class="tab-pane fade" id="newCustomerForm" role="tabpanel" aria-labelledby="newCustomerForm-tab">
      <div class="row mb-1">
        <div class="col-md-6">
          <label for="c_fname" class="form-label">First Name  <span class="text-danger">*</span></label>
          <input name="c_fname" id="firstName" type="text" class="form-control" placeholder="First Name">
          <div class="invalid-feedback">
            Enter first name.
          </div>
        </div>
        <div class="col-md-6">
          <label for="c_lname" class="form-label">Last Name  <span class="text-danger">*</span></label>
          <input name="c_lname" id="lastName" type="text" class="form-control addRequired" placeholder="Last Name">
          <div class="invalid-feedback">
            Enter last name.
          </div>
        </div>
      </div>
      <div class="mb-5">
        <label for="c_company" class="form-label">Company</label>
        <input name="c_company" type="text" class="form-control" placeholder="Company name">
      </div>

      <div class="mb-5">
        <label for="c_email" class="form-label">Email Address <span class="text-danger">*</span></label>
        <input name="c_email" id="emailAddress" type="text" class="form-control addRequired" placeholder="Email address">
        <div class="invalid-feedback">
          Enter email address.
        </div>
      </div>

      <div class="mb-1">
        <label for="c_cphone" class="form-label">Cell <span class="text-danger">*</span></label>
        <input name="c_cphone" id="cellPhone" type="text" class="form-control addRequired" placeholder="Cell number">
        <div class="invalid-feedback">
          Enter cell phone number.
        </div>
      </div>
      <div class="mb-1">
        <label for="c_wphone" class="form-label">Work</label>
        <input name="c_wphone" type="text" class="form-control" placeholder="Work number">
      </div>
      <div class="mb-1">
        <label for="c_hphone" class="form-label">Home</label>
        <input name="c_hphone" type="text" class="form-control" placeholder="Home number">
      </div>
      <div class="mb-5">
        <label for="c_fax" class="form-label">Fax</label>
        <input name="c_fax" type="text" class="form-control" placeholder="Fax number">
      </div>

      <div class="mb-1">
        <label for="c_address1" class="form-label">Address 1</label>
        <input name="c_address1" type="text" class="form-control" placeholder="1234 Main St.">
      </div>
      <div class="mb-1">
        <label for="c_address2" class="form-label">Address 2</label>
        <input name="c_address2" type="text" class="form-control" placeholder="Apartment, studio, or floor">
      </div>
      <div class="row mb-3">
        <div class="col-md-6">
          <label class="form-label" for="c_city">City</label>
          <input type="text" class="form-control" id="c_city" placeholder="City name">
        </div>
        <div class="col-md-3">
          <label for="c_state" class="form-label">State</label>
          <select id="c_state" class="form-select">
            <option selected disabled>State</option>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
          </select>
        </div>
        <div class="col-md-3">
          <label for="c_zip" class="form-label">Zip Code</label>
          <input type="text" class="form-control" id="c_zip" placeholder="00000-0000">
        </div>
      </div>
    </div>
  </div>


</div><!-- End customer info -->

<div id="hiddenInputs">
  <input type="hidden" name="customerOption" id="customerOption" value="exists" />
</div><!-- For storing hidden values -->

<script>
  // Customer option switcher
  function customerOptionSwitch(opt){
    // Change hidden input
    document.getElementById("customerOption").value = opt;
    // Make new customer fields required or remove attribute
    if (opt == 'new'){
      document.getElementById("firstName").setAttribute('required', '');
      document.getElementById("lastName").setAttribute('required', '');
      document.getElementById("emailAddress").setAttribute('required', '');
      document.getElementById("cellPhone").setAttribute('required', '');
    } else if (opt == 'exists') {
      document.getElementById("firstName").removeAttribute('required');
      document.getElementById("lastName").removeAttribute('required');
      document.getElementById("emailAddress").removeAttribute('required');
      document.getElementById("cellPhone").removeAttribute('required');
    }
  }

  // Clear customer search
  function clearSeach() {
    $('#customerSearch').val('');
    $('#customerList').html('');
  }

  // Change customer
  function resetCustomer() {
    clearSeach();
    $('#selectedCustomerDiv').hide(100);
    $('#searchCustomerDiv').show(100);
    $('#searchCancel').show();
    $('#customerSearch').focus();
  }
  // Cancel change
  function resetCustomerCancel() {
    clearSeach();
    $('#searchCustomerDiv').hide(100);
    $('#selectedCustomerDiv').show(100);
  }

  // Select Existing Customer
  function selectExistingCustomer(id, name) {
    $(document).ready(function(){
      $('#searchCustomerDiv').hide();
      $('#selectedCustomerDiv').show();
      $('#selectedCustomerName').html(name);
      $('#selectedCustomerId').val(id);
    });
  }

  // Stop enter key from triggering form
  $(document).keypress(
    function(event){
      if (event.which == '13') {
        event.preventDefault();
      }
  });

  // Ajax for calling customer data
  $(document).ready(function() {
    $('#customerSearch').keyup(function(event) {
      // showing that something is loading
      // $('#response').html("<b>Loading response...</b>");
      /*
       * 'post_receiver.php' - where you will be passing the form data
       * $(this).serialize() - for reading form data quickly
       * function(data){... - data includes the response from post_receiver.php
       */

      // Remove invalid class
      $('#customerSearch').removeClass("is-invalid");

      $.post('assets/customer-search.php', $(this).serialize(), function(data) {
        // demonstrate the response
        $('#customerList').html(data);
        if (event.which == '13') {
          event.preventDefault();
          if($('#customerList').html() != '') {
            selectExistingCustomer($('#highlightedCustomerId').val(), $('#highlightedCustomerName').val());
          } else {
            $('#customerSearch').addClass("is-invalid");
          }
        }
      }).fail(function() {
        //if posting your form fails
        alert("Posting failed.");
      });
      // to restrain from refreshing the whole page, the page
    });
  });
</script>
