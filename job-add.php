
<?php
// Main file

// Get assets
require('assets/start.php');

// Sub navbar
$subnav = array(
  'View Jobs'    => 'jobs.php',
  'Add Job' => 'job-add.php',
);
subnav($subnav);

open_content();

echo page_title("Add Job");
nav_active('crm');

?>

<script>
  $( function() {
    $( "#from" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  $( function() {
    $( "#to" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );

  $( function() {
    $( "#reminderDate" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
</script>

<form action="assets/job-add.php" method="post" class="row needs-validation" autocomplete="off" novalidate>

<div class="container-fluid">
  <div class="row">
    <!-- Job Information -->
    <div class="col-lg-6 mb-3">
      <div class="mb-1">
        <label for="j_name" class="form-label">Job Name <span class="text-danger">*</span></label>
        <input name="j_name" type="text" class="form-control" placeholder="Enter title of job" required autofocus>
        <div class="invalid-feedback">
          Please enter a job name.
        </div>
      </div>
      <div class="mb-3">
        <label for="j_description" class="form-label">Description</label>
        <textarea name="j_description" class="form-control"></textarea>
      </div>
      <div class="mb-1">
        <div class="row">
          <div class="col-md-6">
            <label for="from" class="form-label">Start Date <span class="text-danger">*</span></label>
            <input type="text" id="from" name="j_start" class="form-control" placeholder="Click to add date" required>
            <div class="invalid-feedback">
              Select a start date.
            </div>
          </div>
          <div class="col-md-6">
            <label for="to" class="form-label">End Date <span class="text-danger">*</span></label>
            <input type="text" id="to" name="j_finish_goal" class="form-control" placeholder="Click to add date" required>
            <div class="invalid-feedback">
              Select date you expect to finish.
            </div>
          </div>
        </div>
      </div>
      <div class="mb-3">
        <div class="row">
          <div class="col-md-6">
            <label for="j_remind_date" class="form-label">Set Reminder Date</label>
            <input type="text" id="reminderDate" name="j_remind_date" class="form-control" placeholder="Click to add date">
          </div>
        </div>
      </div>

      <div class="mb-1">
        <div class="row">
          <div class="col-md-6">
            <label for="cs_id" class="form-label">Job Status</label>
            <select class="form-select" name="cs_id" required>
              <option value="1">Open</option>
              <option value="2" selected>Upcoming</option>
              <option value="3">Cancelled</option>
              <option value="4">Pending</option>
              <option value="5">Late</option>
            </select>
          </div>
        </div>
      </div>
    </div>

    <?php require('assets/customer-form.php'); ?>
  </div>
  <div class="row mb-1">
    <div class="col-2">
      <input type="submit" class="btn btn-primary" value="Submit" />
    </div>
  </div>
</div>

</form>



<?php

validation();

close_content();

// Get footer
require('assets/footer.php');

?>
