<?php
// Jobs

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'View Invoices'    => 'invoices.php',
  'Add Invoice' => 'invoice-add.php',
);
subnav($subnav);

open_content();

echo page_title("Edit: INV-10032");
nav_active('crm');

?>

<style>
    body {
      .d-print-none;
    }
</style>

<script>
  $( function() {
    $( "#invoiceDate" ).datepicker({
      dateFormat: "mm/dd/yy",
      defaultDate: "03/13/2022"
    });
  } );
  $( function() {
    $( "#dueDate" ).datepicker({
      dateFormat: "mm/dd/yy",
      defaultDate: "04/11/2022"
    });
  } );

</script>

<form class="" id="form" novalidate>

<div class="container" id="containerWidth">
  <div class="row mb-3 d-print-none">
    <div class="btn-toolbar justify-content-between" role="toolbar">
      <div class="btn-group me-2" role="group">
        <a class="btn btn-sm btn-outline-secondary" role="button" href="invoice-view.php?i_id=10032"><i class="bi-arrow-left"></i> Cancel</a>
        <input type="reset" class="btn btn-sm btn-secondary float-end" role="button" value="Revert Changes" />
      </div>
      <div class="btn-group float-end" role="group">
        <input type="submit" class="btn btn-sm btn-primary" role="button" value="Save Changes" />
      </div>
    </div>
  </div>

  <div class="row mb-3 d-print-block">
    <div class="row mb-3 border-bottom border-primary border-1">
      <div class="col-lg-6 col-sm-6 w-50">
        <h3 class="invoice-header text-uppercase text-primary">Invoice</h3>
      </div>
      <div class="col-lg-6 col-sm-6 w-50 text-end">
        <h3 class="text-primary input-group"><span class="input-group-text" id="invNumAddon">INV-</span>
          <input type="text" class="form-control" value="10032" placeholder="Ex. 12345" aria-aria-describedby="invNumAddon" onfocus="invNumAlert()" onblur="invNumAlertHide()" required></h3>
          <div id="invNumAlert" style="display: none;">
            <p class="alert alert-warning">Caution: Changing the invoice number will create a new invoice, delete the old one, and may cause some links to break.</p>
          </div>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-lg-6 col-sm-6 w-50">
        <h5>Customer Info</h5>
        <p>Bill to: <span class="text-primary">John Doe</span> <a href="#" class="badge text-primary fw-normal">Change</a></p>
        <p>
          <label for="addr1" class="form-label">Address <a href="#" class="badge text-primary fw-normal">Load Customer Address</a></label>
          <input id="addr1" type="text" class="form-control form-control-sm" name="i_addr1" value="0000 Nowhere St." /><br>
          <input type="text" class="form-control form-control-sm" name="i_addr1" value="Notown, TX 00000-0000" />
        </p>
        <p>
          Invoice Date: <input type="text" id="invoiceDate" name="i_dateadded" class="form-control form-control-sm" value="03/13/2022" placeholder="Click to add date"><br>
          Due Date: <input type="text" id="dueDate" name="i_duedate" class="form-control form-control-sm" value="04/11/2022" placeholder="Click to add date">
        </p>
      </div>
      <div class="col-lg-6 col-sm-6 w-50 text-end " id="changeCustomer">
        <p>
          <strong>Trinity Metalworks, LLC</strong><br>
          10032 SE County Road 4200<br>
          Kerens, TX 75144
        </p>
        <p>
          info@trinitymetalworks.com
        </p>
        <p>
          (000) 123-4567<br>
          (123)456-7890
        </p>
      </div>
    </div>
    <div class="row mb-3">
      <h5>Edit Items</h5>
      <table id="table" class="table text-end">
        <thead>
          <tr>
            <th scope="col" class="text-start">Description</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
            <th scope="col">Amount</th>
          </tr>
        </thead>
        <tbody>
          <tr id="tr_1">
            <td class="text-start">
              24' Panel
            </td>
            <td>
              <div class="input-group">
                <input type="text" class="form-control text-end" name="#" id="ili_1" value="10" placeholder="Enter quantity" />
                <button type="button" class="btn btn-outline-secondary" onclick="changeQuantity(this.id, 'add')" id="ili_1"><i class="bi-plus"></i></button>
                <button type="button" class="btn btn-outline-secondary" onclick="changeQuantity(this.id, 'sub')" id="ili_1"><i class="bi-dash"></i></button>
              </div>
              <a class="badge text-primary" href="#!" onclick="removeItem(tr_1)" value="ili_1">Remove</a>
            </td>
            <td>$400.00</td>
            <td>$4,000.00</td>
          </tr>
          <tr id="tr_2">
            <td class="text-start">
              Cattle Guard<br>
              <span class="badge text-secondary">8x16' cattle guard</span>
            </td>
            <td>
              <div class="input-group">
                <input type="text" class="form-control text-end" name="#" id="ili_2" value="10" placeholder="Enter quantity" />
                <button type="button" class="btn btn-outline-secondary" onclick="changeQuantity(this.id, 'add')" id="ili_2"><i class="bi-plus"></i></button>
                <button type="button" class="btn btn-outline-secondary" onclick="changeQuantity(this.id, 'sub')" id="ili_2"><i class="bi-dash"></i></button>
              </div>
              <a class="badge text-primary" href="#!" onclick="removeItem(tr_2)" value="ili_2">Remove</a>
            </td>
            <td>$2,000.00</td>
            <td>$4,000.00</td>
          </tr>
          <tr id="tr_3">
            <td class="text-start" colspan="2">
              <input type="text" class="form-control" name="#" id="ili_3" value="Gate repair" placeholder="Leave blank to remove." />
              <input type="text" class="form-control form-control-sm" name="desc_3" value="8x16' cattle guard" placeholder="Leave blank to remove." />
              <a class="badge text-primary float-end" href="#!" onclick="removeItem(tr_3)" value="ili_2">Remove</a>
            </td>
            <td>$500.00</td>
            <td>$500.00</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Subtotal</td>
            <td>$8,500.00</td>
            <input type="hidden" value="8500.00" id="subtotal" />
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Discount</td>
            <td>
              <p>%<input type="text" class="form-control float-end text-end" style="width:5em" name="i_discount_amount" id="discount_percent" value="5" onkeyup="discountAmount(this.value)" autocomplete="off" /></p>
              <p>Amount:</br>
              $<input type="text" class="form-control float-end text-end" style="width:10em" name="i_discount_amount" id="discount_amount" value="425.0" placeholder="Remove?" autocomplete="off" /></p>
              <span class="badge text-secondary">%<span id="discount_percent_display">5.00</span> <a href="#!"><i class="bi-eye-slash-fill"></i></a></span>
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Sales Tax (%7.35)</td>
            <td>
              $593.50
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Total</td>
            <td>
              $<span id="total_amount" value="8668.50">8,668.50</span>
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td>Paid</td>
            <td>
              $0.00
            </td>
          </tr>
          <tr class="border-bottom border-primary">
            <td colspan="2" class="text-start text-secondary">Thank you for your business!</td>
            <td class="fw-bold text-primary">Balance Due</td>
            <td class="fw-bold">
              $8,668.50
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="row mb-3 text-secondary fst-italic">
      <strong>Customer Notes:</strong><br>
      Thank you very much!
    </div>
  </div>
</div>

<script>
function invNumAlert(){
  $('#invNumAlert').show(300);
}

function invNumAlertHide(){
  $('#invNumAlert').hide(300);
}

$("input[type='text']").on("click", function () {
   $(this).select();
});

// INVOICE FUNCTIONS
function changeQuantity(id, opt){
  currentValue = document.getElementById(id).value;
  if (opt == "add"){
    currentValue = parseInt(currentValue) + 1;
  } else if (opt == "sub") {
    currentValue = parseInt(currentValue) - 1;
  }
  document.getElementById(id).value = currentValue;
}

function removeItem(id){
  table = document.getElementById("table");
  table.deleteRow(id.rowIndex);
}

function currency(num) {
  const dollars = new Intl.NumberFormat(`en-US`, {
      currency: `USD`,
      style: 'currency',
      currencyDisplay: 'Name'
  }).format(num);
  return dollars;
}

function discountAmount(input){
  var percent = input * 0.01; // convert to percent
  var subtotal = document.getElementById("subtotal").value;
  var discount = subtotal * percent;
  discount = currency(discount);
  document.getElementById("discount_amount").value = discount;
}

function discountPercent(input){
  var percent = document.getElementById("i_discount_percent");
  var amount = parseInt(discount.value);
  amount *= input;
  discount.value = amount;
}

</script>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
