<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hypercube</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"
      integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.min.js"></script> -->
    <script src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="images/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="images/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#712cf9">
    <link rel="icon" href="images/favicons/favicon.ico">

    <style>
      :root{
        --text-padding:0.5rem;
      }
      html,
      body {
        font-size: 11pt !important
      }

      #content {
        background-image:url('images/bg.png')
      }
      #sidebar {
        background-image:url('images/dark-bg.png')
      }

      @font-face {
        font-family: 'alegreyaregular';
        src: url('fonts/alegreya-regular-webfont.woff2') format('woff2'),
          url('fonts/alegreya-regular-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
      }

      @font-face {
        font-family: 'playfairdisplay_regular';
        src: url('fonts/playfairdisplay-regular-webfont.woff2') format('woff2'),
          url('fonts/playfairdisplay-regular-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
      }

      body {
        font-family: 'Noto Serif CJK HK';
        font-weight: medium;
      }

      .font-monospace {
        font-family: 'Noto Sans Mono' !important
      }

      .btn,
      input[type=submit],
      .badge,
      .nav-link.active {
        box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);
        background-image: var(--bs-gradient);
        border-radius: 5px;
        border: 1px solid inherit;
        border-bottom-color: rgba(0, 0, 0, 0.5);
        text-decoration: none
      }

      .btn-outline-primary,
      .btn-outline-secondary,
      .btn-outline-danger,
      .btn-outline-success,
      .btn-outline-warning,
      .btn-outline-info,
      .btn-outline-dark {
        box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.5);
        background-color: rgba(255, 255, 255, 0.6);
        background-image: var(--bs-gradient);
      }

      .btn-outline-primary:hover,
      .btn-outline-secondary:hover,
      .btn-outline-danger:hover,
      .btn-outline-success:hover,
      .btn-outline-warning:hover,
      .btn-outline-info:hover,
      .btn-outline-dark:hover {}

      .btn:active {
        margin-top: 2px;
        margin-bottom: -2px;
      }

      .btn-outline-primary:focus,
      .btn-outline-secondary:focus,
      .btn-outline-danger:focus,
      .btn-outline-success:focus,
      .btn-outline-warning:focus,
      .btn-outline-info:focus,
      .btn-outline-dark:focus {}

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        font-family: 'playfairdisplay_regular'
      }

      .form-control,
      .input-group-text,
      .form-select {
        border-color: #aaa
      }

      .form-control,
      .input-group-text,
      .form-select,
      .alert {
        box-shadow: 0 0 0 .15rem rgba(0,0,0,.05);
      }

      .badge,
      .ui-datepicker {
        font-family: 'Noto Sans Mono' !important
      }
      table.table.data-list tr td, table.table.data-list tbody tr th {
        padding:0
      }
      
      table.table.data-list tr td a, table.table.data-list tbody tr th a {
        display:block;
        width:100%;
        height:100%;
        padding:var(--text-padding)
      }
      table.table tr:active {
        background-color:var(--bs-info)
      }
    </style>

    <script>
      $(document).ready(function(){
        $("#btn-filter-status").click(function(){
          $("#table-invoices").slideToggle();
        });

        $("#input-invoice-search").keyup(function() {
          $("#ajax-content").html(
            $(this).val()
          );
        });
      });
    </script>
  </head>

  <body>
    <div class="container-fluid p-0 m-0" id="wrapper">
      <div class="row p-0 m-0" id="wrapper-row">

        <!-- Sidebar Nav -->
        <div class="p-3 text-bg-dark col-lg-2 col-md-3" id="sidebar">
          <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
            <img src="images/logo.png" class="image-fluid d-inline mx-2" width="45" height="auto">
            <span class="fs-4">Hypercube</span>
          </a>
          <hr>
          <ul class="nav nav-pills flex-column mb-auto">
            <li class="nav-item">
              <a href="#" class="nav-link active" aria-current="page">
                <svg class="bi pe-none me-2" width="16" height="16">
                  <use xlink:href="#home"></use>
                </svg>
                Home
              </a>
            </li>
            <li>
              <a href="#" class="nav-link text-white">
                <svg class="bi pe-none me-2" width="16" height="16">
                  <use xlink:href="#speedometer2"></use>
                </svg>
                Dashboard
              </a>
            </li>
            <li>
              <a href="#" class="nav-link text-white">
                <svg class="bi pe-none me-2" width="16" height="16">
                  <use xlink:href="#table"></use>
                </svg>
                Orders
              </a>
            </li>
            <li>
              <a href="#" class="nav-link text-white">
                <svg class="bi pe-none me-2" width="16" height="16">
                  <use xlink:href="#grid"></use>
                </svg>
                Products
              </a>
            </li>
            <li>
              <a href="#" class="nav-link text-white">
                <svg class="bi pe-none me-2" width="16" height="16">
                  <use xlink:href="#people-circle"></use>
                </svg>
                Customers
              </a>
            </li>
          </ul>
          <hr>
          <div class="dropdown">
            <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle"
              data-bs-toggle="dropdown" aria-expanded="false">
              <img src="https://github.com/mdo.png" alt="" class="rounded-circle me-2" width="32" height="32">
              <strong>mdo</strong>
            </a>
            <ul class="dropdown-menu dropdown-menu-dark text-small shadow" style="">
              <li><a class="dropdown-item" href="#">User Settings</a></li>
              <li><a class="dropdown-item" href="#">Messages
                  <span class="badge bg-danger rounded-pill">4</span>
                </a>
              </li>
              <li><a class="dropdown-item" href="#">Profile</a></li>
              <li>
                <hr class="dropdown-divider">
              </li>
              <li><a class="dropdown-item" href="#">Sign out</a></li>
            </ul>
          </div>
        </div><!-- end of sidebar -->
        
        <div class="col-lg-10 col-md-9 px-0">
          <div class="container-fluid m-0" id="content">

            <div class="row border-1 border-bottom mb-2 bg-light text-dark">
              <ul class="nav nav-pills">
                <li>
                  <a href="#!" class="nav-link rounded-0 active">List All Invoices</a>
                </li>
                <li>
                  <a href="#!" class="nav-link text-reset">New Invoice</a>
                </li>
                <li>
                  <a href="#!" class="nav-link text-reset">New Customer</a>
                </li>
              </ul>
            </div>

            <div class="row">
              <div class="col" id="content-title">
                <h1>Invoices</h1>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-3 col-md-3" id="invoice-filters">
                <input type="text" id="input-invoice-search" name="input-invoice-search" class="form-control form-control-sm" placeholder="Search invoice IDs, customers, etc." autofocus>
              </div>

              <div class="col-lg-2 col-md-2">
                <div class="input-group">
                  <select class="form-select form-select-sm" id="invoice-filter-status">
                    <option disabled selected>Filter by status</option>
                    <option value="1">Draft</option>
                    <option value="2">Unpaid</option>
                    <option value="3">Paid</option>
                    <option value="4">Abandoned</option>
                  </select>
                  <button id="btn-filter-status" class="btn btn-sm btn-primary">
                    Go
                  </button>
                </div>
              </div>
            </div>

            <div class="row"><!-- Invoices -->
              <div class="col-12" id="table-invoices">
                <table class="table data-list table-striped table-hover font-monospace">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">
                        <a href="#!" class="text-reset" id="invoice-orderby-customer">Customer <i class="bi-arrow-down-up"></i></a>
                      </th>
                      <th scope="col">
                        <a href="#!" class="text-reset" id="invoice-orderby-amount">Amount <i class="bi-arrow-down-up"></i></a>
                      </th>
                      <th scope="col">
                        <a href="#!" class="text-reset" id="invoice-orderby-date">Date <i class="bi-arrow-down-up"></i></a>
                      </th>
                      <th scope="col">
                        <a href="#!" class="text-reset" id="invoice-orderby-due">Due <i class="bi-arrow-down-up"></i></a>
                      </th>
                      <th scope="col">
                        <a href="#!" class="text-reset" id="invoice-orderby-status">Status <i class="bi-arrow-down-up"></i></a>
                      </th>
                    </tr>
                  </thead>
                  <tbody class="border">
                    <tr>
                      <th scope="row"><a href="#!" class="text-reset text-decoration-none">1</a> </th>
                      <td><a href="#!" class="text-reset text-decoration-none">Mark Otto</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">$10000</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><span class="text-secondary"><a href="#!" class="text-reset text-decoration-none">Draft</a></span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#!" class="text-reset text-decoration-none">1</a> </th>
                      <td><a href="#!" class="text-reset text-decoration-none">Mark Otto</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">$10000</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><span class="text-secondary"><a href="#!" class="text-reset text-decoration-none">Draft</a></span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#!" class="text-reset text-decoration-none">1</a> </th>
                      <td><a href="#!" class="text-reset text-decoration-none">Mark Otto</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">$10000</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><span class="text-secondary"><a href="#!" class="text-reset text-decoration-none">Draft</a></span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#!" class="text-reset text-decoration-none">1</a> </th>
                      <td><a href="#!" class="text-reset text-decoration-none">Mark Otto</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">$10000</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><span class="text-secondary"><a href="#!" class="text-reset text-decoration-none">Draft</a></span></td>
                    </tr>
                    <tr>
                      <th scope="row"><a href="#!" class="text-reset text-decoration-none">1</a> </th>
                      <td><a href="#!" class="text-reset text-decoration-none">Mark Otto</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">$10000</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><a href="#!" class="text-reset text-decoration-none">3/12/1992</a></td>
                      <td><span class="text-success"><a href="#!" class="text-reset text-decoration-none">Paid</a></span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div><!-- invoices -->

          </div>
        </div>
      </div><!-- end of wrapper -->
  </body>

</html>