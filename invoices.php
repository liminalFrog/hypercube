<?php
// Main file

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'All Invoices'    => 'invoices.php',
  'View Drafts'    => 'invoices.php?v=draft',
  'View Unpaid' => 'invoices.php?v=unpaid',
  'View Paid' => 'invoices.php?v=paid',
  'New Invoice' => 'invoice-add.php'
);
subnav($subnav);

open_content();

echo page_title("Invoices");
nav_active('crm');

?>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3">
    <div class="col-md-3">
      <input type="text" class="form-control form-control-sm" id="invoiceSearch" placeholder="Search Invoices (Inv #, customer name)" oninput="invoiceSearch(this.value)" autofocus>
    </div>
    <div class="col-md-3">
      <div class="input-group input-group-sm flex-nowrap">
        <span class="input-group-text" id="selectedActionLabel">With selected:</span>
        <select id="selectedAction" class="form-select form-select-sm" aria-describedby="selectedActionLabel">
          <option value="paid">Mark as Paid</option>
        </select>
        <button class="btn btn-sm btn-outline-secondary">Go &rarr;</button>
      </div>
    </div>
    <div class="col-md-3">

    </div>
    <div class="col-md-3 d-flex">
      <a href="invoice-add.php" class="btn btn-sm btn-outline-secondary ms-auto"><i class="bi-file-earmark-plus"></i> New Invoice</a>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col"><a class="text-dark" href="invoices.php?order=id" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by ID"># <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col"><a class="text-dark" href="invoices.php?order=customer" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by customer name">Last, First <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col"><a class="text-dark" href="invoices.php?order=duedate" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by due date">Due Date <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col"><a class="text-dark" href="invoices.php?order=amount" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by amount">Amount <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col"><a class="text-dark" href="invoices.php?order=status" data-bs-toggle="tooltip" data-bs-placement="top" title="Order by status">Status <i class="bi-arrow-down-up"></i></a></th>
            <th scope="col">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="selectAll" onclick="selectAll()">
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
	  <td><a href="invoice-view.php?i_id=1">INV-<?php echo $i_id; ?></a></td>
            <td>Doe, John</td>
            <td>12/13/2022</td>
            <td>$1250.50</td>
            <td><span class="badge bg-dark">Abandoned</span></td>
            <td><input class="form-check-input" type="checkbox" id="0123" /></td>
          </tr>
          <tr>
            <td colspan="6" class="text-muted text-center">No results</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <!-- Pagination -->
  <div class="row">
    <div class="col-md-6">
      <nav aria-label="...">
        <ul class="pagination">
          <li class="page-item disabled">
            <span class="page-link"><i class="bi-arrow-left-circle"></i></span>
          </li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item active" aria-current="page">
            <span class="page-link">2</span>
          </li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#"><i class="bi-arrow-right-circle"></i></a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>

<script>
// Ajax for calling invoice data
$(document).ready(function() {
  $('#invoiceSearch').keyup(function(event) {
    // showing that something is loading
    // $('#response').html("<b>Loading response...</b>");
    /*
     * 'post_receiver.php' - where you will be passing the form data
     * $(this).serialize() - for reading form data quickly
     * function(data){... - data includes the response from post_receiver.php
     */

    // Remove invalid class
    $('#invoiceSearch').removeClass("is-invalid");

    $.post('assets/invoice-search.php', $(this).serialize(), function(data) {
      // demonstrate the response
      $('#invoiceList').html(data);
      // if (event.which == '13') {
      //   event.preventDefault();
      //   if($('#invoiceList').html() != '') {
      //     selectExistinginvoice($('#highlightedinvoiceId').val(), $('#highlightedinvoiceName').val());
      //   } else {
      //     $('#invoiceSearch').addClass("is-invalid");
      //   }
      // }
    }).fail(function() {
      //if posting your form fails
      alert("Posting failed.");
    });
    // to restrain from refreshing the whole page, the page
  });
});
</script>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
