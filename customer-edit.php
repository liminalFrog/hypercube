
<?php
// Main file

// Get assets
require('assets/start.php');

// Sub navbar
$subnav = array(
  'View Customers'    => 'customers.php',
  'Add Customer' => 'customer-add.php',
);
subnav($subnav);

open_content();

echo page_title("Edit: John Doe");
nav_active('crm');

?>

<form action="assets/customer-save.php" method="post">

<div class="container-fluid">
  <div class="row mb-2">
    <div class="col">
      <a href="customer-view.php?c_id=1"><i class="bi-arrow-left"></i> Cancel</a>
    </div>
  </div>

  </div>
  <div class="row">
    <div class="col-lg-6 mb-3">
      <div class="row mb-1">
        <div class="col-md-6">
          <label for="c_fname" class="form-label">First Name  <span class="text-danger">*</span></label>
          <input name="c_fname" type="text" class="form-control" placeholder="First Name" value="John" autofocus>
        </div>
        <div class="col-md-6">
          <label for="c_lname" class="form-label">Last Name  <span class="text-danger">*</span></label>
          <input name="c_lname" type="text" class="form-control" placeholder="Last Name" value="Doe">
        </div>
      </div>
      <div class="mb-5">
        <label for="c_company" class="form-label">Company</label>
        <input name="c_company" type="text" class="form-control" placeholder="Company name" value="jDoe, Inc.">
      </div>

      <div class="mb-5">
        <label for="c_email" class="form-label">Email Address <span class="text-danger">*</span></label>
        <input name="c_email" type="text" class="form-control" placeholder="Email address" value="jdoe@example.com">
      </div>

      <div class="mb-1">
        <label for="c_cphone" class="form-label">Cell <span class="text-danger">*</span></label>
        <input name="c_cphone" type="text" class="form-control" placeholder="Cell number" value="1234567890">
      </div>
      <div class="mb-1">
        <label for="c_wphone" class="form-label">Work</label>
        <input name="c_wphone" type="text" class="form-control" placeholder="Work number" value="0123456789">
      </div>
      <div class="mb-1">
        <label for="c_hphone" class="form-label">Home</label>
        <input name="c_hphone" type="text" class="form-control" placeholder="Home number" value="0123456789">
      </div>
      <div class="mb-5">
        <label for="c_fax" class="form-label">Fax</label>
        <input name="c_fax" type="text" class="form-control" placeholder="Fax number" value="0123456789">
      </div>

      <div class="mb-1">
        <label for="c_address1" class="form-label">Address 1</label>
        <input name="c_address1" type="text" class="form-control" placeholder="1234 Main St." value="jDoe Headquarters">
      </div>
      <div class="mb-1">
        <label for="c_address2" class="form-label">Address 2</label>
        <input name="c_address2" type="text" class="form-control" placeholder="Apartment, studio, or floor" value="0000 Nowhere St.">
      </div>
      <div class="row mb-3">
        <div class="col-md-6">
          <label class="form-label" for="c_city">City</label>
          <input type="text" class="form-control" id="c_city" placeholder="City name" value="Notown">
        </div>
        <div class="col-md-3">
          <label for="c_state" class="form-label">State</label>
          <select id="c_state" class="form-select">
            <option disabled>State</option>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX" selected>Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
          </select>
        </div>
        <div class="col-md-3">
          <label for="c_zip" class="form-label">Zip Code</label>
          <input type="text" class="form-control" id="c_zip" placeholder="00000-0000" value="12345">
        </div>
      </div>

    </div>
    <div class="col-lg-6">
      <div class="mb-5">
        <label for="c_notes" class="form-label">Notes</label>
        <textarea class="form-control" aria-label="Notes" name="c_notes">These are the notes.</textarea>
      </div>
      <div class="row mb-5">
        <label for="cs_id" class="form-label">Status</label>
        <select name="cs_id" class="form-control">
          <option value="" disabled>Status</option>
          <option value="1" selected>Active</option>
          <option value="2">Inactive</option>
          <option value="3">Pending</option>
          <option value="4">Deceased</option>
          <option value="5">Blacklisted</option>
        </select>
      </div>
    </div>
  </div>
  <div class="row mb-1">
    <div class="col-2">
      <input type="submit" class="form-control btn-primary" value="Save Changes" />
    </div>
  </div>
</div>

</form>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
