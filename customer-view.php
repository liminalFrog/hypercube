<?php
// Main file

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'View Customers'    => 'customers.php',
  'Add Customer' => 'customer-add.php',
);
subnav($subnav);

open_content();

echo page_title("John Doe");
nav_active('crm');

?>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3">
    <div class="col-md-12">
      <a href="job-add.php?c_id=1" class="btn btn-sm btn-outline-secondary"><i class="bi-clipboard2-plus"></i> New Job</a>
      <a href="event-add.php" class="btn btn-sm btn-outline-secondary"><i class="bi-person"></i> New Event</a>
      <a href="customer-edit.php" class="btn btn-sm btn-outline-secondary"><i class="bi-pencil"></i> Edit Customer</a>
    </div>
  </div>

  <div class="row">
    <!-- Information -->
    <div class="col-lg-6">
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col" colspan="2">Customer Data:</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="col">Name</th>
            <td>John Doe</td>
          </tr>
          <tr>
            <th scope="col">Status</th>
            <td><span class="badge bg-success">Active</span></td>
          </tr>
          <tr>
            <th scope="col">Company</th>
            <td>jDoe, Inc.</td>
          </tr>
          <tr>
            <th scope="col">Email</th>
            <td><a href="mailto:jdoe@example.com">jdoe@example.com</a></td>
          </tr>
          <tr>
            <th scope="col">Cell</th>
            <td>+1 (234) 567-890</td>
          </tr>
          <tr>
            <th scope="col">Home</th>
            <td>+1 (234) 567-890</td>
          </tr>
          <tr>
            <th scope="col">Work</th>
            <td>+1 (234) 567-890</td>
          </tr>
          <tr>
            <th scope="col">Fax</th>
            <td>(234) 567-890</td>
          </tr>
          <tr>
            <th scope="col">Address</th>
            <td>
              <address>
                <strong>jDoe Headquarters</strong><br />
                0000 Nowhere St.<br />
                Notown, TX 00000
              </address>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- Jobs Notes -->
    <div class="col-lg-6">
      <div class="row">
        <strong>Jobs Open</strong>
        <ul class="list-unstyled">
          <li><a href="job-view.php?j_id=2">2x Cattle Guards</a> <span class="badge bg-danger">Late</span></li>
          <li><a href="job-view.php?j_id=3">Entryway w/ r-gate</a></li>
        </ul>
      </div>
      <div class="row">
        <strong>Notes</strong>
        <p>
          These are the notes.
        </p>
      </div>
    </div>
  </div>

</div>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
