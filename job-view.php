<?php
// Jobs

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'View Jobs'    => 'jobs.php',
  'Add Job' => 'job-add.php',
);
subnav($subnav);

open_content();

echo page_title("30 Head Corral & Panels");
nav_active('crm');

?>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3">
    <div class="input-group">
      <a role="button" class="btn btn-sm btn-outline-secondary" href="jobs.php">Back to Jobs</a>
      <a role="button" class="btn btn-sm btn-outline-secondary" href="job-edit.php?j_id=1">Edit Job</a>
    </div>
  </div>
  <div class="row mb-3"><!-- Main container row -->
    <!-- Information -->
    <div class="col-md-6">
      <div class="row mb-3">
          <table class="table table-striped w-100">
            <thead>
              <tr>
                <th scope="col" colspan="2">Job Information</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Status</td>
                <td><span class="badge bg-primary">Open</span></td>
              </tr>
              <tr>
                <td>Customer</td>
                <td><a href="customer-view.php?id=1&j_id=1">John Doe</a></td>
              </tr>
              <tr>
                <td>Invoice</td>
                <td><a href="invoice-view.php?i_id=10032">INV-10032</a> <span class="badge bg-primary">Pending Payment</span></td>
              </tr>
              <tr>
                <td>Timeline</td>
                <td>
                  May 14 - May 21
                  <br /><span class="text-danger">3 days left</span>
                </td>
              </tr>
              <tr>
                <td>Finish Date</td>
                <td><span class="text-muted">Pending</span></td>
              </tr>
              <tr>
                <td colspan="2"><strong>Notes/Description</strong><br />
                  This is the description.<br />
                  1. This is an item<br>
                  2. This is another item<br>
                  Might finish late!
                </td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
    <!-- Customer Info/Events -->
    <div class="col-md-6">
      <div class="row mb-3">
        <div class="col">
          <strong><i class="bi-person"></i> Customer Events</strong>
          <div class="list-group">
            <a role="button" class="list-group-item list-group-item-action text-success border-bottom-0" data-bs-toggle="modal" data-bs-target="#exampleModal">
              <i class="bi-check-square"></i> Confirm Order with Customer
              <br><span class="badge bg-light text-muted">Monday, March 20, 2022 - 3:30 PM</span>
            </a>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm Order with Customer</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <span class="badge bg-light text-muted">Monday, March 20, 2022 - 3:30 PM</span><br>
                    <strong>Notes:</strong><br>
                    These are the notes<br>
                    We discussed stuff.<br>
                    We talked about a lot of stuff then.  He was cranky.  I didn't like him much, but it's ok.  Maybe next time he won't be such a jerk.
                  </div>
                  <div class="modal-footer">
                    Mark as:&nbsp;
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal" onclick="markCancelled(1)">Cancelled</button>
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal" onclick="markMissed(1)">Missed</button>
                    <button type="button" class="btn btn-sm btn-success" data-bs-dismiss="modal" onclick="markComplete(1)">Completed</button>
                  </div>
                </div>
              </div>
            </div>
            <a class="list-group-item list-group-item-action text-success" href="event-view.php?ce_id=1">
              <i class="bi-check-square"></i> Follow-up phone call
              <br><span class="badge bg-light text-secondary">Monday, March 25, 2022 - 3:30 PM</span>
            </a>
            <a class="list-group-item list-group-item-action text-danger" href="event-view.php?ce_id=1">
              <i class="bi-x-square"></i> Follow-up 2 <span class="badge bg-danger">Missed</span>
              <br><span class="badge bg-light text-danger">Monday, March 25, 2022 - 3:30 PM</span>
            </a>
            <a class="list-group-item list-group-item-action text-secondary" href="event-view.php?ce_id=1">
              <i class="bi-slash-square"></i> Follow-up 2 <span class="badge bg-secondary">Cancelled</span>
              <br><span class="badge bg-light text-secondary">Monday, March 25, 2022 - 3:30 PM</span>
            </a>
            <a class="list-group-item list-group-item-action text-success" href="event-view.php?ce_id=1">
              <i class="bi-check-square"></i> Follow-up phone call 2
              <br><span class="badge bg-light text-secondary">Monday, March 25, 2022 - 3:30 PM</span>
            </a>
            <a class="list-group-item list-group-item-action" href="event-view.php?ce_id=1">
              <i class="bi-square"></i> Send pics of status
              <br><span class="badge bg-light text-secondary">Monday, March 30, 2022 - 3:30 PM</span>
            </a>
            <a class="list-group-item list-group-item-action text-muted" href="#" onclick="newEvent()">
              <i class="bi-plus-square"></i> Click to add new event...
            </a>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <hr />
  <div class="row">
    <div class="col-12">
      <h4>Images</h4>

    </div>
  </div>
</div>



<?php

close_content();

// Get footer
require('assets/footer.php');

?>
