<?php
// Jobs

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'View Invoices'    => 'invoices.php',
  'Add Invoice' => 'invoice-add.php',
);
subnav($subnav);

open_content();

echo page_title("Add Invoice");
nav_active('crm');

?>

<style>
    body {
      .d-print-none;
    }
</style>

<script>
  $( function() {
    $( "#invoiceDate" ).datepicker({
      dateFormat: "mm/dd/yy",
      defaultDate: "03/13/2022"
    });
  } );
  $( function() {
    $( "#dueDate" ).datepicker({
      dateFormat: "mm/dd/yy",
      defaultDate: "04/11/2022"
    });
  } );

  // Format currency
  function currency(num) {
    const amount = parseFloat(num, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    return amount;
  }

</script>

<form class="" method="post" action="assets/test.php" id="form" novalidate>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3 d-print-none">
    <div class="btn-toolbar justify-content-between" role="toolbar">
      <div class="btn-group me-2" role="group">
        <a class="btn btn-sm btn-outline-secondary" role="button" href="invoice-view.php?i_id=10032"><i class="bi-arrow-left"></i> Cancel</a>
        <input type="reset" class="btn btn-sm btn-secondary float-end" role="button" value="Revert Changes" />
      </div>
      <div class="btn-group float-end" role="group">
        <input type="submit" class="btn btn-sm btn-primary" role="button" value="Save Changes" />
      </div>
    </div>
  </div>

  <div class="row mb-3 d-print-block">
    <div class="row mb-3 border-bottom border-primary border-1">
      <div class="col-lg-6 col-sm-6 w-50">
        <h3 class="invoice-header text-uppercase text-primary">Invoice</h3>
      </div>
      <div class="col-lg-6 col-sm-6 w-50 text-end">
        <h3 class="text-primary input-group"><span class="input-group-text" id="invNumAddon">INV-</span>
          <input type="text" class="form-control form-control-sm" value="10032" placeholder="Ex. 12345" aria-aria-describedby="invNumAddon" required>
        </h3>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-md-7">
        <div class="row mb-3">
          <div class="col-md-6">
            Invoice Date: <input type="text" id="invoiceDate" name="i_remind_date" class="form-control form-control-sm" value="03/13/2022" placeholder="Click to add date"><br>
          </div>
          <div class="col-md-6">
            Due Date: <input type="text" id="dueDate" name="j_remind_date" class="form-control form-control-sm" value="04/11/2022" placeholder="Click to add date">
          </div>
        </div>
        <h5>Add Products</h5>
        <p>
          <a class="btn btn-sm btn-outline-secondary" role="button" href="#" data-bs-toggle="modal" data-bs-target="#productModal"><i class="bi-box"></i> Add Product</a>
        </p>

        <!-- Add Product modal -->
        <div class="modal fade" id="productModal" tabindex="-1" aria-labelledby="addProductModal" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="addProductModal">Add Product</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div id="productSearchDiv">
                    <label for="productSearch" class="form-label">
                      Search Products
                    </label>
                    <input type="text" value="" name="productSearch" class="form-control mb-3" placeholder="Search products or custom name..." id="productSearch" autocomplete="off" autofocus>
                  </div>
                  <div id="selectedProductDiv" style="display:none;">
                    <label class="form-label" for="selectedProductName">Selected Product</label>
                    <input type="text" class="form-control text-primary" id="selectedProductName" value="" disabled>
                    <input type="hidden" id="selectedProductId" value="">
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col">
                    <div id="productList" class="list-group">
                      <!-- products go here -->
                    </div>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-6">
                    <label for="productQuantity" class="form-label">
                      Quantity
                    </label>
                    <div class="input-group">
                      <input type="text" class="form-control form-control-sm text-end" name="product_quantity" id="productQuantity" value="1" placeholder="Enter quantity" />
                      <button type="button" class="btn btn-sm btn-outline-secondary" onclick="changeQuantity(this.id, 'add')" id="productQuantity"><i class="bi-plus"></i></button>
                      <button type="button" class="btn btn-sm btn-outline-secondary" onclick="changeQuantity(this.id, 'sub')" id="productQuantity"><i class="bi-dash"></i></button>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="row mb-3">
                      <label for="productQuantity" class="form-label">
                        Price
                      </label>
                      <input type="text" name="product_quantity" value="" class="form-control form-control-sm" id="productPrice" autocomplete="off">
                    </div>
                    <div class="row mb-3">
                      <div class="btn-group btn-group-sm" role="group">
                        <input type="radio" role="button" class="btn-check reg" id="radioPrice1" name="radioPrice" autocomplete="off" value="reg" onclick="productPriceType(this.value)" checked>
                        <label for="radioPrice1" class="btn btn-outline-primary">Regular</label>
                        <input type="radio" role="button" class="btn-check whole" id="radioPrice2" name="radioPrice" value="whole" autocomplete="off" onclick="productPriceType(this.value)">
                        <label for="radioPrice2" class="btn btn-outline-primary">Wholesale</label>
                        <input type="radio" role="button" class="btn-check list" id="radioPrice3" name="radioPrice" value="list" autocomplete="off" onclick="productPriceType(this.value)">
                        <label for="radioPrice3" class="btn btn-outline-primary">List</label>
                      </div>
                      <script>
                        function productPriceType(type) {
                          alert(type);
                          alert($('.whole').val());
                        }
                      </script>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-success" data-bs-dismiss="modal" onclick="addProduct(this.value)">Add Product</button>
              </div>
            </div>
          </div>
        </div>

        <script>
          // Immediately focus on the search bar when opened
          document.getElementById("productModal").addEventListener('shown.bs.modal', () => {
            document.getElementById("productSearch").focus()
          })
        </script>
        <style>
          /* Quantity buttons get mixed up with dropdown calendars */
          .input-group {
            z-index: 1;
          }
        </style>
        <table id="invoiceTable" class="table table-sm text-end">
          <thead>
            <tr>
              <th scope="col" class="text-start">Description</th>
              <th scope="col">Quantity</th>
              <th scope="col">Price</th>
              <th scope="col">Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr id="tr_1">
              <td class="text-start">
                24' Panel
              </td>
              <td>
                <div class="input-group">
                  <input type="text" class="form-control form-control-sm text-end" name="#" id="ili_1" value="10" placeholder="Enter quantity" />
                  <button type="button" class="btn btn-sm btn-outline-secondary" onclick="changeQuantity(this.id, 'add')" id="ili_1"><i class="bi-plus"></i></button>
                  <button type="button" class="btn btn-sm btn-outline-secondary" onclick="changeQuantity(this.id, 'sub')" id="ili_1"><i class="bi-dash"></i></button>
                </div>
                <a class="badge text-primary" href="#!" onclick="removeProduct(tr_1)" value="ili_1">Remove</a>
              </td>
              <td>$400.00</td>
              <td>$4,000.00</td>
            </tr>
            <tr id="tr_2">
              <td class="text-start">
                Cattle Guard<br>
                <span class="badge text-secondary">8x16' cattle guard</span>
              </td>
              <td>
                <div class="input-group">
                  <input type="text" class="form-control form-control-sm text-end" name="#" id="ili_2" value="10" placeholder="Enter quantity" />
                  <button type="button" class="btn btn-sm btn-outline-secondary" onclick="changeQuantity(this.id, 'add')" id="ili_2"><i class="bi-plus"></i></button>
                  <button type="button" class="btn btn-sm btn-outline-secondary" onclick="changeQuantity(this.id, 'sub')" id="ili_2"><i class="bi-dash"></i></button>
                </div>
                <a class="badge text-primary" href="#!" onclick="removeProduct(tr_2)" value="ili_2">Remove</a>
              </td>
              <td>$2,000.00</td>
              <td>$4,000.00</td>
            </tr>

            <tr>
              <td colspan="2"></td>
              <td>Subtotal</td>
              <td>$8,500.00</td>
              <input type="hidden" value="8500.00" id="subtotal" />
            </tr>
            <tr>
              <td colspan="2"></td>
              <td>Discount</td>
              <td>
                <p>%<input type="text" class="form-control form-control-sm float-end text-end" style="width:5em" name="i_discount_amount" id="discount_percent" value="5" placeholder="Remove?" onchange="discountAmount(this.value)" autocomplete="off" /></p>
                <p>Amount:</br>
                $<input type="text" class="form-control form-control-sm float-end text-end" style="width:10em" name="i_discount_amount" id="discount_amount" value="425.0" placeholder="Remove?" autocomplete="off" /></p>
                <span class="badge text-secondary">%<span id="discount_percent_display">5.00</span> <a href="#!"><i class="bi-eye-slash-fill"></i></a></span>
              </td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td>Sales Tax (%7.35)</td>
              <td>
                $593.50
              </td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td>Total</td>
              <td>
                $8,668.50
                <input type="hidden"  id="total_amount" value="8668.50" />
              </td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td>Paid</td>
              <td>
                $0.00
              </td>
            </tr>
            <tr class="border-bottom border-primary">
              <td colspan="2" class="text-start text-secondary">Thank you for your business!</td>
              <td class="fw-bold text-primary">Balance Due</td>
              <td class="fw-bold" id="balanceDue">
              </td>
              <script>
                $('#balanceDue').html(currency(8668.5));
              </script>
            </tr>
          </tbody>
        </table>
      </div>

      <?php require('assets/customer-form.php'); ?>
    </div>
  </div>
</div>

</form>

<script>

$("input[type='text']").on("click", function () {
   $(this).select();
});

// INVOICE FUNCTIONS
function changeQuantity(id, opt){
  currentValue = document.getElementById(id).value;
  if (opt == "add"){
    currentValue = parseInt(currentValue) + 1;
  } else if (opt == "sub") {
    currentValue = parseInt(currentValue) - 1;
  }
  document.getElementById(id).value = currentValue;
}

function removeProduct(id){
  table = document.getElementById("table");
  table.deleteRow(id.rowIndex);
}

function discountAmount(input){
  var percent = input * 0.01; // convert to percent
  var subtotal = document.getElementById("subtotal").value;
  var discount = subtotal * percent;
  discount = currency(discount);
  document.getElementById("discount_amount").value = discount;
}

function discountPercent(input){
  var percent = document.getElementById("i_discount_percent");
  var amount = parseInt(discount.value);
  amount *= input;
  discount.value = amount;
}

// Stop enter key from triggering form
$(document).keypress(
  function(event){
    if (event.which == '13') {
      event.preventDefault();
    }
});

$(document).keypress(
  function(event){
    if (event.which == 107 || event.which == 61){
      $('#productModal').modal("show");
    }
});

// Ajax for calling product data
$(document).ready(function() {
  $('#productSearch').keyup(function(event) {
    // showing that something is loading
    // $('#response').html("<b>Loading response...</b>");
    /*
     * 'post_receiver.php' - where you will be passing the form data
     * $(this).serialize() - for reading form data quickly
     * function(data){... - data includes the response from post_receiver.php
     */

    // Remove invalid class
    $('#productSearch').removeClass("is-invalid");

    $.post('assets/product-search.php', $(this).serialize(), function(data) {
      // demonstrate the response
      $('#productList').html(data);
      if (event.which == '13') {
        event.preventDefault();
        if($('#productList').html() != '') {
          selectExistingProduct($('#highlightedProductId').val(), $('#highlightedProductName').val());
        } else {
          $('#productSearch').addClass("is-invalid");
        }
      }
    }).fail(function() {
      //if posting your form fails
      alert("Posting failed.");
    });
    // to restrain from refreshing the whole page, the page
  });
});

// Select Existing Product
function selectExistingProduct(id, name, price, wholesale, list) {
  $(document).ready(function(){
    $('#productSearch').val(""); // Clear input
    $('#productSearchDiv').hide(); // Hide input
    $('#productList').html(''); // Clear product search list
    $('#selectedProductDiv').show(); // Show selected name
    $('#selectedProductName').val(name);
    $('#selectedProductId').val(id); // Save for adding to invoice
    $('#productPrice').val(currency(price)); // Get product price
  });
}

// When closing or cancelling the add product modal
// Wipe for future product
const productModal = document.getElementById('productModal')
productModal.addEventListener('hidden.bs.modal', event => {
  $('#productSearch').val(""); // Clear input
  $('#productList').html(''); // Clear product search list
  $('#selectedProductId').val(''); // Clear selected product id
  $('#selectedProductName').val(''); // Clear selected product name
  $('#selectedProductDiv').hide();
  $('#productSearchDiv').show(); // Show empty input
});

// Add selected product to invoice
function addToInvoice(id, name, quantity, price){

}

</script>

<?php

close_content();

// Get footer
require('assets/footer.php');

?>
