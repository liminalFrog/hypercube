<?php
// Jobs

// Get assets
require('assets/start.php');


// Sub navbar
$subnav = array(
  'Products Database'    => 'products.php',
  'Add Product' => 'product-add.php',
);
subnav($subnav);

open_content();

echo page_title("Products Database");
nav_active('products');

?>

<div class="container-fluid" id="containerWidth">
  <div class="row mb-3">
    <div class="col-md-6">
      <div class="input-group">
        <a role="button" class="btn btn-sm btn-outline-secondary" href="product-add.php">+<i class="bi-box-seam"></i> Add Product</a>
      </div>
    </div>
    <div class="col-md-6">
      <input type="text" class="form-control" id="searchProducts" onchange="searchProducts(this.value)" placeholder="Search products" autocomplete="off" autofocus />
    </div>
  </div>

  <div class="row mb-3">
    <div class="col-md-9">
      <table class="table table-striped" id="tableContent">
        <thead>
          <tr>
            <th scope="col">
              Img
            </th>
            <th scope="col">
              Name
            </th>
            <th scope="col">
              Category
            </th>
            <th scope="col">
              Price
            </th>
            <th scope="col">
              Quantity
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <img src="images/blank.png" class="img-fluid p-0 m-0" style="height:2em" />
            </td>
            <td>
              <a href="product-view.php?p_id=1">Product Name</a>
            </td>
            <td>
              <a href="setting-pcat.php?pcat_id=1">Uncategorized</a>
            </td>
            <td>
              $200.00
            </td>
            <td>
              20
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-md-3">
      <h5>Last Edited</h5>
      <div class="list-group">
        <a href="product-view.php?p_id=1" class="list-group-item">
          Product 1
        </a>
        <a href="product-view.php?p_id=2" class="list-group-item">
          Product 2
        </a>
        <a href="product-view.php?p_id=3" class="list-group-item">
          Product 3
        </a>
      </ul>
    </div>
  </div>

</div>



<?php

close_content();

// Get footer
require('assets/footer.php');

?>
