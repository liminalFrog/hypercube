<?php
// Jobs

// Get assets
require('assets/start.php');

open_content();

page_title("Litany | Error 404");

?>

<div class="container">
  <div class="row mb-3">
    <div class="col-5 py-5 mx-auto">
      <h1 class="display-3 mb-0 fw-bold">404</h1>
      <h1>Page not found</h1>
      <p>Looks like the page you're looking for doesn't exist.</p>
      <p><a href="index.php" role="button" class="btn btn-primary">Go Home</a></p>
    </div>
  </div>
</div>


<?php

close_content();

// Get footer
require('assets/footer.php');

?>
